package fr.cnam.td2moca.algos;

import java.util.ArrayList;
import java.util.Stack;

import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * Parcours en profondeur
 * 
 * @author Christopher MILAZZO
 *
 */
public class ParcoursDFSAlgo implements Algorithme {
    
    private Graphe graphe;
    private ArrayList<Sommet> listSommets;
    private Sommet sommetDepart;

    /**
     * Constructeur
     * 
     * @param graphe le graphe sur lequel appliquer l'algorithme de parcours DFS
     * @param sommetDepart le seommet à partir du quel on commence le parcours
     * @param stackSommets la pile qui doit être valorisée par l'algorithme
     */
    public ParcoursDFSAlgo(Graphe graphe, Sommet sommetDepart, ArrayList<Sommet> listSommets) {
	this.graphe = graphe;
	this.listSommets = listSommets;
	this.sommetDepart = sommetDepart;
    }

    @Override
    public void executer() {
	Stack<Sommet> stack = new Stack<Sommet>();
	DFS(graphe, stack, listSommets, this.sommetDepart);
    }
    
    private void DFS(final Graphe graphe, final Stack<Sommet> stack, final ArrayList<Sommet> stackTrace, final Sommet sommetCourant) {
	stack.add(sommetCourant);
	stackTrace.add(sommetCourant);
	for (Sommet sommet : graphe.getSuccesseurs(sommetCourant)) {
	    if (!stackTrace.contains(sommet) ) {
		DFS(graphe, stack, stackTrace, sommet);
	    }
	}
	stack.pop();
    }

}
