package fr.cnam.td2moca.algos;

import java.util.LinkedList;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * Parcous en largeur
 * 
 * @author Christopher MILAZZO
 *
 */
public class ParcoursBFSAlgo implements Algorithme {
    
    private Graphe graphe;
    private LinkedList<Sommet> sommetsQueue;

    public ParcoursBFSAlgo(Graphe graphe, Sommet sommetDepart, LinkedList<Sommet> listeSommets) {
	this.graphe = graphe;
	this.sommetsQueue = listeSommets;
	sommetsQueue.add(sommetDepart);
    }

    @Override
    public void executer() {
	BFS(graphe, sommetsQueue, 0);
    }

    /**
     * Fonction recurcive de parcours en largeur de tous les sommets d'un graphe
     * donné. A la fin du traitement, la liste chainé "sommetsQueue" sera valorisée
     * avec les sommets visités dans l'ordre du parcours.
     * 
     * @param graphe       le graphe sur lequel le parcous doit se faire
     * @param sommetsQueue une liste chainée qui doit contenir le sommet de départ
     * @param head         pour le premier appel il doit être valorisé à 0
     */
    private void BFS(final Graphe graphe, final LinkedList<Sommet> sommetsQueue, int head) {
	for (Sommet sommet : graphe.getSuccesseurs(sommetsQueue.get(head))) {
	    if (!sommetsQueue.contains(sommet)) {
		sommetsQueue.add(sommet);
	    }
	}
	if (head < sommetsQueue.size() - 1) {
	    BFS(graphe, sommetsQueue, ++head);
	}
	return;
    }

}
