package fr.cnam.td2moca.algos;

import java.util.ArrayList;
import java.util.Comparator;

import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;

public class KruskalAlgo implements Algorithme {

    @SuppressWarnings("unused")
    private ArrayList<Arc> cheminLePlusCourt;
    private Graphe graphe;
    
    /**
     * Constructeur
     * 
     * @param cheminLePlusCourt la liste des arcs qui doit être valorisé par l'algorithme de Kruskal
     */
    public KruskalAlgo(Graphe graphe, ArrayList<Arc> cheminLePlusCourt) {
	this.cheminLePlusCourt = cheminLePlusCourt;
	this.graphe = graphe;
    }

    @Override
    public void executer() {
	cheminLePlusCourt.addAll(kruskal());
    }

    private ArrayList<Arc> kruskal() {
	// 1 Créer un graphe T vide
	Graphe T = new Graphe("kruskal");
	
	// 2 Créer la liste des arcs et les ordonnée par ordre croissant de pondération
	ArrayList<Arc> arcs = new ArrayList<Arc>();
	arcs.addAll(graphe.getListeArcs());
	arcs.sort(new Comparator<Arc>() {
	    @Override
	    public int compare(Arc a1, Arc a2) {
		if (a1.getValeurPonderation() < a2.getValeurPonderation()) {
		    return -1;
		} else if (a1.getValeurPonderation() > a2.getValeurPonderation()) {
		    return 1;
		} else {
		    return 0;
		}
	    }
	});
	// 3 Itérer sur la liste des arcs
	for (Arc arc : arcs) {
	    // 3.1 Si les deux sommets de l'arc courant sont dans le nouveau graphe T
	    if (T.getListeSommets().contains(arc.getOrigine()) && T.getListeSommets().contains(arc.getDestination())) {
		// 3.1.1 Si ces deux sommets ne sont pas connectés dans T, alors ajouter l'arc à T
		if (!T.areSommetsConnectes(arc.getOrigine(), arc.getDestination())) {
		    T.ajouterArc(arc);
		}// 3.1.2 sinon ne rien faire
	    } else {
		// 3.2 sinon ajouter l'arc courant dans T et le/les sommet(s) non déjà présent(s) dans T
		T.ajouterArc(arc);
		if (!T.getListeSommets().contains(arc.getOrigine())) {		    
		    T.ajouterSommet(arc.getOrigine());
		}
		if (!T.getListeSommets().contains(arc.getDestination())) {		    
		    T.ajouterSommet(arc.getDestination());
		}
	    }
	    
	}
	
	return T.getListeArcs();
    }
}
