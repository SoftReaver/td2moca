package fr.cnam.td2moca.algos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class ColoriageAlgo implements Algorithme{
    /* Les couleurs disponibles pour la coloration */
    private Color[] colors = { // \
	    new Color(252, 186, 3), // Orange \
	    new Color(62, 194, 97), // Vert \
	    new Color(222, 73, 205), // Violet \
	    new Color(22, 125, 199), // Bleu \
	    new Color(247, 64, 244), // Rose \
	    new Color(237, 215, 47), // Jaune \
	    new Color(82, 80, 70) // Gris \
    };

    /* Affectation des couleurs par sommet */
    private HashMap<Sommet, Color> mapColoration;
    private Graphe graphe;

    /**
     * Constructeur
     * 
     * @param graphe le graphe sur lequel applicquer l'algorithme
     * @param mapColoration la hashmap à valoriser par l'algorithme de coloriage
     */
    public ColoriageAlgo(Graphe graphe, HashMap<Sommet, Color> mapColoration) {
	this.graphe = graphe;
	this.mapColoration = mapColoration;
    }

    /**
     * Algorithme de coloriage. Celui-ci agit directement sur le panneau graphique
     * pour appliquer des couleurs sur les sommets.
     * 
     * L'algorithme peut lever l'exception ColorOverflowException en cas de
     * dépassement de la liste des couleurs disponibles.
     */
    @Override
    public void executer() {
	affecterCouleursAuxSommets();
    }

    /**
     * Valorise la map d'affectation des couleurs "mapColoration"
     */
    private void affecterCouleursAuxSommets() {
	for (Sommet sommet : this.graphe.getListeSommets()) {
	    Color couleur = recupererPlusPetiteCouleurDispo(recupererCouleursVoisines(sommet));
	    mapColoration.put(sommet, couleur);
	}
    }

    private ArrayList<Color> recupererCouleursVoisines(Sommet sommet) {
	ArrayList<Color> couleursVoisines = new ArrayList<Color>();
	ArrayList<Sommet> sommetsVoisins = this.graphe.getSommetsAdjacents(sommet);
	for (Sommet sommetVoisin : sommetsVoisins) {
	    Color couleur = this.mapColoration.get(sommetVoisin);
	    if (couleur != null && !couleursVoisines.contains(couleur)) {
		couleursVoisines.add(couleur);
	    }
	}
	return couleursVoisines;
    }

    private Color recupererPlusPetiteCouleurDispo(ArrayList<Color> couleursVoisines) {
	for (int i = 0; i < this.colors.length; i++) {
	    if (!couleursVoisines.contains(this.colors[i])) {
		return this.colors[i];
	    }
	}
	throw new ColorOverflowException();
    }

    public class ColorOverflowException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ColorOverflowException() {
	    super("Error while executing coloration algorithm : Not enought color available.");
	}
    }

    
}
