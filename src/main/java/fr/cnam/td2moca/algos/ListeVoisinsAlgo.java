package fr.cnam.td2moca.algos;

import java.util.ArrayList;
import java.util.stream.Collectors;

import fr.cnam.td2moca.gui.VHSInformationPanel;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class ListeVoisinsAlgo implements Algorithme {
    
    private Graphe graphe;
    
    public ListeVoisinsAlgo(Graphe graphe) {
	this.graphe = graphe;
    }

    public void executer() {
	ArrayList<Sommet> sommets = graphe.getListeSommets();
	String[][] data = new String[sommets.size()][(graphe.isOriente()) ? 3 : 2];
	String[] headers = new String[(graphe.isOriente()) ? 3 : 2];
	headers[0] = "Sommets";

	if (graphe.isOriente()) {
	    headers[1] = "Successeurs";
	    headers[2] = "Predecesseurs";
	} else {
	    headers[1] = "Voisins";
	}

	for (int i = 0; i < sommets.size(); i++) {
	    data[i][0] = sommets.get(i).getLabel();
	    data[i][1] = graphe.getSuccesseurs(sommets.get(i)).stream().map(Sommet::getLabel)
		    .collect(Collectors.joining(", "));
	    if (graphe.isOriente()) {
		data[i][2] = graphe.getPredecesseurs(sommets.get(i)).stream().map(Sommet::getLabel)
			.collect(Collectors.joining(", "));
	    }
	}
	new VHSInformationPanel(data, headers);
    }
}
