package fr.cnam.td2moca;

import java.util.Stack;

import fr.cnam.td2moca.commandes.Commande;

/**
 * @author Christopher MILAZZO
 */
public class Invocateur {

    private Main context;
    private Stack<Commande> historiqueCommandesExecuter;
    private Stack<Commande> historiqueCommandesDefaire;

    public Invocateur(Main context) {
	this.historiqueCommandesExecuter = new Stack<Commande>();
	this.historiqueCommandesDefaire = new Stack<Commande>();
	this.context = context;
    }

    public void executer(Commande commande) {
	this.historiqueCommandesExecuter.clear();
	this.historiqueCommandesDefaire.push(commande);
	commande.executer();
	this.context.setNeedToSave(true);
	this.context.getPanneauGraphique().removeShapesColors();
	return;
    }

    public void annuler() {
	if (this.historiqueCommandesDefaire.isEmpty() == false) {
	    this.historiqueCommandesExecuter.push(this.historiqueCommandesDefaire.pop());
	    this.historiqueCommandesExecuter.peek().defaire();
	    this.context.setNeedToSave(true);
	    this.context.getPanneauGraphique().removeShapesColors();
	}
    }

    public void refaire() {
	if (this.historiqueCommandesExecuter.isEmpty() == false) {
	    this.historiqueCommandesDefaire.push(this.historiqueCommandesExecuter.pop());
	    this.historiqueCommandesDefaire.peek().executer();
	    this.context.setNeedToSave(true);
	    this.context.getPanneauGraphique().removeShapesColors();
	}
    }

    public boolean hasProchaineCommande() {
	return this.historiqueCommandesExecuter.size() > 0;
    }

    public boolean hasCommandePrecedente() {
	return this.historiqueCommandesDefaire.size() > 0;
    }

    public String nomProchaineCommande() {
	if (this.hasProchaineCommande()) {
	    return this.historiqueCommandesExecuter.peek().toString();
	} else {
	    return "";
	}
    }

    public String nomCommandePrecedente() {
	if (this.hasCommandePrecedente()) {
	    return this.historiqueCommandesDefaire.peek().toString();
	} else {
	    return "";
	}
    }

    public void effacerHistorique() {
	this.historiqueCommandesDefaire.clear();
	this.historiqueCommandesExecuter.clear();
    }
}