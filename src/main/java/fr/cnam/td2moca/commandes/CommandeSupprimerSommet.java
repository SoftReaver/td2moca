package fr.cnam.td2moca.commandes;

import java.util.ArrayList;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class CommandeSupprimerSommet implements Commande {

    private Sommet sommet;
    private Graphe graphe;
    private PanneauGraphique panneauGraphique;
    private ArrayList<Arc> arcsASupprimer;

    public CommandeSupprimerSommet(Sommet sommet, Graphe graphe, PanneauGraphique panneauGraphique) {
	this.sommet = sommet;
	this.graphe = graphe;
	this.panneauGraphique = panneauGraphique;
	this.arcsASupprimer = new ArrayList<Arc>();
	for (Arc arc : this.graphe.getListeArcs()) {
	    if (arc.getOrigine().equals(sommet) || arc.getDestination().equals(sommet)) {
		this.arcsASupprimer.add(arc);
	    }
	}
    }

    public void executer() {
	this.graphe.supprimerSommet(this.sommet);
	this.panneauGraphique.erase(this.sommet);
	for (Arc arc : this.arcsASupprimer) {
	    new CommandeSupprimerArc(arc, this.graphe, this.panneauGraphique).executer();
	}
    }

    public void defaire() {
	this.graphe.ajouterSommet(this.sommet);
	this.panneauGraphique.draw(this.sommet);
	for (Arc arc : this.arcsASupprimer) {
	    new CommandeAjouterArc(arc, this.graphe, this.panneauGraphique).executer();
	}
    }

    @Override
    public String toString() {
	return "Supprimer le sommet " + this.sommet.getLabel();
    }
}
