package fr.cnam.td2moca.commandes;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;

/**
 * @author Christopher MILAZZO
 */
public class CommandeModifierArc implements Commande {
    private Arc ancienArc;
    private Arc nouvelArc;
    private Graphe graphe;
    private PanneauGraphique panneauGraphique;

    public CommandeModifierArc(Arc ancienArc, Arc nouvelArc, Graphe graphe, PanneauGraphique panneauGraphique) {
	this.graphe = graphe;
	this.nouvelArc = nouvelArc;
	this.ancienArc = ancienArc;
	this.panneauGraphique = panneauGraphique;
    }

    public void executer() {
	this.panneauGraphique.erase(this.ancienArc);
	this.graphe.supprimerArc(this.ancienArc);

	if (this.nouvelArc.getOrigine().isVisible() && this.nouvelArc.getDestination().isVisible()) {
	    this.panneauGraphique.draw(this.nouvelArc);
	}
	this.graphe.ajouterArc(this.nouvelArc);
    }

    public void defaire() {
	this.panneauGraphique.erase(this.nouvelArc);
	this.graphe.supprimerArc(this.nouvelArc);

	if (this.ancienArc.getOrigine().isVisible() && this.ancienArc.getDestination().isVisible()) {
	    this.panneauGraphique.draw(this.ancienArc);
	}
	this.graphe.ajouterArc(this.ancienArc);
    }

    @Override
    public String toString() {
	return "Modifier l'arc " + this.ancienArc;
    }
}
