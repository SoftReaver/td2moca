package fr.cnam.td2moca.commandes;

import fr.cnam.td2moca.model.Graphe;

public class CommandeRenommerGraphe implements Commande {

    private Graphe graphe;
    private String nouvNom;
    private String ancienNom;

    public CommandeRenommerGraphe(Graphe graphe, String nouvNom) {
	this.graphe = graphe;
	this.nouvNom = nouvNom;
	this.ancienNom = graphe.getNom();
    }

    @Override
    public void executer() {
	this.graphe.setNom(this.nouvNom);
    }

    @Override
    public void defaire() {
	this.graphe.setNom(this.ancienNom);
    }

    @Override
    public String toString() {
	return "Renommer graphe";
    }
}
