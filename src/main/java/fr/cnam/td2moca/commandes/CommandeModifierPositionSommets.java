package fr.cnam.td2moca.commandes;

import java.util.ArrayList;

import fr.cnam.td2moca.model.Sommet;

public class CommandeModifierPositionSommets implements Commande {

    private ArrayList<Sommet> sommets;
    private int transX;
    private int transY;

    public CommandeModifierPositionSommets(int oldPX, int oldPY, int newPX, int newPY, ArrayList<Sommet> sommets) {
	this.sommets = new ArrayList<Sommet>(sommets);
	this.transX = newPX - oldPX;
	this.transY = newPY - oldPY;
    }

    @Override
    public void executer() {
	for (Sommet sommet : sommets) {
	    sommet.x += this.transX;
	    sommet.y += this.transY;
	}
    }

    @Override
    public void defaire() {
	for (Sommet sommet : sommets) {
	    sommet.x -= this.transX;
	    sommet.y -= this.transY;
	}
    }

    @Override
    public String toString() {
	if (this.sommets.size() > 1) {
	    return "Déplacer " + this.sommets.size() + " sommets";
	} else {
	    return "Déplacer sommet " + this.sommets.get(0).getLabel();
	}
    }
}
