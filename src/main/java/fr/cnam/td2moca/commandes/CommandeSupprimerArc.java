package fr.cnam.td2moca.commandes;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;

/**
 * @author Christopher MILAZZO
 */
public class CommandeSupprimerArc implements Commande {

    private Arc arc;
    private Graphe graphe;
    private PanneauGraphique panneauGraphique;

    public CommandeSupprimerArc(Arc arc, Graphe graphe, PanneauGraphique panneauGraphique) {
	this.arc = arc;
	this.graphe = graphe;
	this.panneauGraphique = panneauGraphique;
    }

    public void executer() {
	this.graphe.supprimerArc(this.arc);
	this.panneauGraphique.erase(arc);
    }

    public void defaire() {
	this.graphe.ajouterArc(this.arc);
	this.panneauGraphique.draw(arc);
    }

    @Override
    public String toString() {
	return "Supprimer l'arc " + this.arc;
    }
}
