package fr.cnam.td2moca.commandes;

import fr.cnam.td2moca.model.Graphe;

public class CommandeDefinirArcsPonderes implements Commande {
    private Graphe graphe;
    private boolean pondere;

    public CommandeDefinirArcsPonderes(Graphe graphe, boolean pondere) {
	this.graphe = graphe;
	this.pondere = pondere;
    }

    @Override
    public void executer() {
	this.graphe.setPonderationArc(pondere);
    }

    @Override
    public void defaire() {
	this.graphe.setPonderationArc(!pondere);
    }

    @Override
    public String toString() {
	return this.pondere ? "Graphe pondéré" : "Graphe non-pondéré";
    }
}
