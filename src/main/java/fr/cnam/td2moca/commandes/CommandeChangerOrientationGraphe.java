package fr.cnam.td2moca.commandes;

import fr.cnam.td2moca.model.Graphe;

public class CommandeChangerOrientationGraphe implements Commande {
    private Graphe graphe;
    private boolean oriente;

    public CommandeChangerOrientationGraphe(Graphe graphe, boolean oriente) {
	this.graphe = graphe;
	this.oriente = oriente;
    }

    @Override
    public void executer() {
	this.graphe.setOriente(oriente);
    }

    @Override
    public void defaire() {
	this.graphe.setOriente(!oriente);
    }

    @Override
    public String toString() {
	return this.oriente ? "Graphe orienté" : "Graphe non-orienté";
    }
}
