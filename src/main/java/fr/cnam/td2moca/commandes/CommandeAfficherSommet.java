package fr.cnam.td2moca.commandes;

import java.util.ArrayList;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class CommandeAfficherSommet implements Commande {

    private Graphe graphe;
    private PanneauGraphique panneauGraphique;
    private Sommet sommet;
    private ArrayList<Arc> arcADessiner;

    public CommandeAfficherSommet(Sommet sommet, Graphe graphe, PanneauGraphique panneauGraphique) {
	this.graphe = graphe;
	this.panneauGraphique = panneauGraphique;
	this.sommet = sommet;
	this.arcADessiner = new ArrayList<Arc>();

	for (Arc arc : this.graphe.getListeArcs()) {
	    if ((arc.getOrigine().equals(sommet) && arc.getDestination().isVisible())
		    || (arc.getDestination().equals(sommet) && arc.getOrigine().isVisible())) {
		if (!arcADessiner.contains(arc)) {
		    this.arcADessiner.add(arc);
		}
	    }
	}
    }

    public void executer() {
	sommet.setVisible(true);
	this.panneauGraphique.draw(sommet);

	for (Arc arc : this.arcADessiner) {
	    this.panneauGraphique.draw(arc);
	}
    }

    public void defaire() {
	sommet.setVisible(false);
	this.panneauGraphique.erase(sommet);

	for (Arc arc : this.arcADessiner) {
	    this.panneauGraphique.erase(arc);
	}
    }

    @Override
    public String toString() {
	return "Afficher sommet " + this.sommet.getLabel();
    }
}
