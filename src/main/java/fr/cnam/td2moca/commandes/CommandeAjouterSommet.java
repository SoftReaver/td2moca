package fr.cnam.td2moca.commandes;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class CommandeAjouterSommet implements Commande {

    private Sommet sommet;
    private Graphe graphe;
    private PanneauGraphique panneauGraphique;

    public CommandeAjouterSommet(Sommet sommet, Graphe graphe, PanneauGraphique panneauGraphique) {
	this.sommet = sommet;
	this.graphe = graphe;
	this.panneauGraphique = panneauGraphique;
    }

    public void executer() {
	this.graphe.ajouterSommet(this.sommet);
	this.panneauGraphique.draw(sommet);
    }

    public void defaire() {
	this.graphe.supprimerSommet(this.sommet);
	this.panneauGraphique.erase(sommet);
    }

    @Override
    public String toString() {
	return "Ajouter le sommet " + this.sommet;
    }
}
