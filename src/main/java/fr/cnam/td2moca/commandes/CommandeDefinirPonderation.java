package fr.cnam.td2moca.commandes;

import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;

public class CommandeDefinirPonderation implements Commande {
    private Graphe graphe;
    private Arc arc;
    private float oldPonderation;
    private float ponderation;

    public CommandeDefinirPonderation(Graphe graphe, Arc arc, float ponderation) {
	this.oldPonderation = arc.getValeurPonderation();
	this.ponderation = ponderation;
	this.graphe = graphe;
	this.arc = arc;
    }

    @Override
    public void executer() {
	this.graphe.definirValeurPonderation(this.arc, this.ponderation);
    }

    @Override
    public void defaire() {
	this.graphe.definirValeurPonderation(this.arc, this.oldPonderation);
    }

    @Override
    public String toString() {
	return "Definir ponderation";
    }
}
