package fr.cnam.td2moca.commandes;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;

/**
 * @author Christopher MILAZZO
 */
public class CommandeAjouterArc implements Commande {

    private Arc arc;
    private Graphe graphe;
    private PanneauGraphique panneauGraphique;

    public CommandeAjouterArc(Arc arc, Graphe graphe, PanneauGraphique panneauGraphique) {
	this.arc = arc;
	this.graphe = graphe;
	this.panneauGraphique = panneauGraphique;
    }

    public void executer() {
	this.graphe.ajouterArc(this.arc);
	if (arc.getOrigine().isVisible() && arc.getDestination().isVisible()) {
	    this.panneauGraphique.draw(arc);
	}
    }

    public void defaire() {
	this.graphe.supprimerArc(this.arc);
	this.panneauGraphique.erase(arc);
    }

    @Override
    public String toString() {
	return "Ajouter l'arc " + this.arc;
    }
}
