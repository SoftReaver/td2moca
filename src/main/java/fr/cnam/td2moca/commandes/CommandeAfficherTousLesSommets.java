package fr.cnam.td2moca.commandes;

import java.util.ArrayList;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class CommandeAfficherTousLesSommets implements Commande {

    private Graphe graphe;
    private PanneauGraphique panneauGraphique;
    private ArrayList<Sommet> sommetsADessiner;
    private ArrayList<Arc> arcADessiner;

    public CommandeAfficherTousLesSommets(Graphe graphe, PanneauGraphique panneauGraphique) {
	this.graphe = graphe;
	this.panneauGraphique = panneauGraphique;
	this.sommetsADessiner = new ArrayList<Sommet>();
	this.arcADessiner = new ArrayList<Arc>();

	for (Sommet sommet : this.graphe.getListeSommets()) {
	    if (!sommet.isVisible()) {
		this.sommetsADessiner.add(sommet);
	    }
	}
	for (Arc arc : this.graphe.getListeArcs()) {
	    if (sommetsADessiner.contains(arc.getOrigine()) || sommetsADessiner.contains(arc.getDestination())) {
		if (!arcADessiner.contains(arc)) {
		    this.arcADessiner.add(arc);
		}
	    }
	}
    }

    public void executer() {
	for (Sommet sommet : this.sommetsADessiner) {
	    sommet.setVisible(true);
	    this.panneauGraphique.draw(sommet);
	}
	for (Arc arc : this.arcADessiner) {
	    this.panneauGraphique.draw(arc);
	}
    }

    public void defaire() {
	for (Sommet sommet : this.sommetsADessiner) {
	    sommet.setVisible(false);
	    this.panneauGraphique.erase(sommet);
	}
	for (Arc arc : this.arcADessiner) {
	    this.panneauGraphique.erase(arc);
	}
    }

    @Override
    public String toString() {
	return "Afficher tous les sommets";
    }
}
