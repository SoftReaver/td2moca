package fr.cnam.td2moca.commandes;

/**
 * 
 */
public interface Commande {

    /* Doit modifier l'état du système en fonction de la commande */
    public void executer();

    /*
     * Doit remettre l'état du système comme il était avant l'éxecution de la
     * commande
     */
    public void defaire();

    /*
     * La commande doit redéfinir la méthode toString pour être afficher
     * correctement dans le menu
     */
    String toString();

}
