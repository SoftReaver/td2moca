package fr.cnam.td2moca.commandes;

import java.util.ArrayList;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class CommandeCacherSommet implements Commande {

    private PanneauGraphique panneauGraphique;
    private Sommet sommet;
    private ArrayList<Arc> arcsACacher;
    private Graphe graphe;

    public CommandeCacherSommet(Sommet sommet, Graphe graphe, PanneauGraphique panneauGraphique) {
	this.graphe = graphe;
	this.panneauGraphique = panneauGraphique;
	this.sommet = sommet;
	this.arcsACacher = new ArrayList<Arc>();

	for (Arc arc : this.graphe.getListeArcs()) {
	    if (arc.getOrigine().equals(this.sommet) || arc.getDestination().equals(this.sommet)) {
		this.arcsACacher.add(arc);
	    }
	}
    }

    public void executer() {
	sommet.setVisible(false);
	this.panneauGraphique.erase(sommet);

	for (Arc arc : this.arcsACacher) {
	    this.panneauGraphique.erase(arc);
	}
    }

    public void defaire() {
	sommet.setVisible(true);
	this.panneauGraphique.draw(sommet);

	for (Arc arc : this.arcsACacher) {
	    this.panneauGraphique.draw(arc);
	}
    }

    @Override
    public String toString() {
	return "Cacher sommet " + this.sommet.getLabel();
    }
}
