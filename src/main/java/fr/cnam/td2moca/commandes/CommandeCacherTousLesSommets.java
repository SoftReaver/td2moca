package fr.cnam.td2moca.commandes;

import java.util.ArrayList;

import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class CommandeCacherTousLesSommets implements Commande {

    private PanneauGraphique panneauGraphique;
    private ArrayList<Sommet> sommetsACacher;
    private ArrayList<Arc> arcsACacher;
    private Graphe graphe;

    public CommandeCacherTousLesSommets(Graphe graphe, PanneauGraphique panneauGraphique) {
	this.graphe = graphe;
	this.panneauGraphique = panneauGraphique;
	this.sommetsACacher = new ArrayList<Sommet>();
	this.arcsACacher = new ArrayList<Arc>();

	for (Sommet sommet : this.graphe.getListeSommets()) {
	    if (sommet.isVisible()) {
		this.sommetsACacher.add(sommet);
	    }
	}
	for (Arc arc : this.graphe.getListeArcs()) {
	    if (this.sommetsACacher.contains(arc.getOrigine()) || this.sommetsACacher.contains(arc.getDestination())) {
		this.arcsACacher.add(arc);
	    }
	}
    }

    public void executer() {
	for (Sommet sommet : this.sommetsACacher) {
	    sommet.setVisible(false);
	    this.panneauGraphique.erase(sommet);
	}
	for (Arc arc : this.arcsACacher) {
	    this.panneauGraphique.erase(arc);
	}
    }

    public void defaire() {
	for (Sommet sommet : this.sommetsACacher) {
	    sommet.setVisible(true);
	    this.panneauGraphique.draw(sommet);
	}
	for (Arc arc : this.arcsACacher) {
	    this.panneauGraphique.draw(arc);
	}
    }

    @Override
    public String toString() {
	return "Cacher tous les sommets";
    }
}
