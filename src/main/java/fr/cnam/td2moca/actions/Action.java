package fr.cnam.td2moca.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;

/**
 * Représentation d'une action effectué sur l'IHM
 * 
 * @author Christopher MILAZZO
 */
public abstract class Action implements ActionListener {

    private Invocateur invocateur;
    protected Main context;

    protected Action(Invocateur invocateur, Main context) {
	Objects.requireNonNull(invocateur);
	Objects.requireNonNull(context);
	this.invocateur = invocateur;
	this.context = context;
    }

    public final void actionPerformed(ActionEvent e) {
	Commande commande = this.creerCommande();
	if (commande != null) {
	    this.invocateur.executer(commande);
	}
	this.context.sync();
    }

    public final void perform() {
	this.perform("Action anonyme");
    }

    public final void perform(String actionName) {
	this.actionPerformed(new ActionEvent(this, 0, actionName));
    }

    @Override
    public String toString() {
	return "Action anonyme";
    }

    /**
     * La classe fille doit implémenter cette méthode. Celle-ci doit renvoyer une
     * Commande liée à cette action. Si aucune action doit être historisée, il est
     * possible de retourner null.
     * 
     * @return une commande ou null
     * @see Commande
     */
    protected abstract Commande creerCommande();
}