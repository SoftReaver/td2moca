package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;

public class ActionQuitter extends Action {

    private Invocateur invocateur;

    public ActionQuitter(Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.invocateur = invocateur;
    }

    @Override
    protected Commande creerCommande() {
	if (this.context.isNeedToSave()) {
	    new ActionFermer(this.invocateur, this.context).creerCommande();
	    if (this.context.isNeedToSave()) {
		return null;
	    }
	}
	this.context.exit();
	return null;
    }
}
