package fr.cnam.td2moca.actions;

import javax.swing.JOptionPane;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.CommandeRenommerGraphe;
import fr.cnam.td2moca.commandes.Commande;

public class ActionRenommerGraphe extends Action {
    public ActionRenommerGraphe(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	String nouvNom = JOptionPane.showInputDialog("Nom du graphe", this.context.getGraphe().getNom());
	if (nouvNom == null) {
	    return null;
	} else {
	    return new CommandeRenommerGraphe(this.context.getGraphe(), nouvNom);
	}
    }

}
