package fr.cnam.td2moca.actions;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.algos.ParcoursDFSAlgo;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class ActionParcoursDFSAlgo extends Action{
    
    private JComboBox<Sommet> listeSommets;
    private JDialog frame;
    private Sommet sommetDepart;

    public ActionParcoursDFSAlgo(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	this.sommetDepart = null;
	Graphe graphe = this.context.getGraphe().getPartiel();
	
	showWindow();
	
	if (this.sommetDepart == null) {
	    this.sommetDepart = graphe.getListeSommets().get(0);
	}
	
	ArrayList<Sommet> listSommets = new ArrayList<Sommet>();
	new ParcoursDFSAlgo(graphe, this.sommetDepart, listSommets).executer();
	
	PanneauGraphique panneauGraphique = this.context.getPanneauGraphique();
	new Thread(new Runnable() {  
	    @Override
	    public void run() {
		panneauGraphique.removeSommetsColors();
		for (Sommet sommet : listSommets) {
		    panneauGraphique.colorShape(sommet, Color.DARK_GRAY);
		    try {
			Thread.sleep(500);
		    } catch (InterruptedException silent) {}
		}
	    }
	}).start();
	
	JOptionPane.showMessageDialog(null, String.format("<html><body><p style='width: 200px;'>Les sommets ont été parcourcus dans l'ordre suivant :<br> %s</p></body></html>", listSommets));
	return null;
    }

    protected void setSommet(Sommet sommet) {
	this.sommetDepart = sommet;
    }
    
    protected JComboBox<Sommet> getListeSommets() {
	return this.listeSommets;
    }
    
    private void showWindow() {
	this.frame = new JDialog(this.context.getFrame(), "Sommet de départ", true);
	FlowLayout layout = new FlowLayout();
	Container container = new Container();
	container.setLayout(layout);

	this.listeSommets = new JComboBox<Sommet>();

	for (Sommet sommet : this.context.getGraphe().getListeSommets()) {
	    this.listeSommets.addItem(sommet);
	}

	JButton okButton = new JButton("OK");
	okButton.addActionListener(new OkAction(this, frame));

	container.add(this.listeSommets);
	container.add(okButton);

	frame.add(container);
	frame.setBounds(120, 120, 200, 150);
	frame.setResizable(false);
	frame.setVisible(true);
    }

    private class OkAction implements ActionListener {
	private ActionParcoursDFSAlgo actionParcoursDFSAlgo;
	private JDialog frame;

	public OkAction(ActionParcoursDFSAlgo action, JDialog frame) {
	    this.actionParcoursDFSAlgo = action;
	    this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	    this.actionParcoursDFSAlgo.setSommet((Sommet) this.actionParcoursDFSAlgo.getListeSommets().getSelectedItem());
	    this.frame.dispose();
	}
    }
}
