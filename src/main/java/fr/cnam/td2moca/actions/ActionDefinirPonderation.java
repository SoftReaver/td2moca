package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeDefinirPonderation;
import fr.cnam.td2moca.model.Arc;

public class ActionDefinirPonderation extends Action {
    private Arc arc;
    private float ponderation;

    public ActionDefinirPonderation(Invocateur invocateur, Main context, Arc arc, float ponderation) {
	super(invocateur, context);
	this.arc = arc;
	this.ponderation = ponderation;
    }

    @Override
    protected Commande creerCommande() {
	return new CommandeDefinirPonderation(this.context.getGraphe(), this.arc, this.ponderation);
    }
}
