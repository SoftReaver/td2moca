package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeAjouterSommet;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class ActionAjouterSommet extends Action {
    public ActionAjouterSommet(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    public Commande creerCommande() {
	Graphe graphe = this.context.getGraphe();
	int maxID = 0;
	int currentID = 0;
	for (Sommet sommet : graphe.getListeSommets()) {
	    if ((currentID = Integer.parseInt(sommet.getLabel())) > maxID) {
		maxID = currentID;
	    }
	}
	Sommet sommet = new Sommet("" + (maxID + 1));
	sommet.setVisible(true);

	sommet.x = this.context.getPositionXNouvSommet();
	sommet.y = this.context.getPositionYNouvSommet();

	return new CommandeAjouterSommet(sommet, graphe,
		((PanneauGraphique) this.context.getComponentsRefMap().get(Main.PANNEAU_GRAPHIQUE_ID)));
    }
}