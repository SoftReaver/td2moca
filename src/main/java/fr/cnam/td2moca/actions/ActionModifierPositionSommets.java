package fr.cnam.td2moca.actions;

import java.util.ArrayList;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeModifierPositionSommets;
import fr.cnam.td2moca.model.Sommet;

public class ActionModifierPositionSommets extends Action {

    private ArrayList<Sommet> sommets;
    private int oldPX;
    private int oldPY;
    private int newPX;
    private int newPY;

    public ActionModifierPositionSommets(int oldPX, int oldPY, int newPX, int newPY, final ArrayList<Sommet> sommets,
	    Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.sommets = sommets;
	this.oldPX = oldPX;
	this.oldPY = oldPY;
	this.newPX = newPX;
	this.newPY = newPY;
    }

    @Override
    protected Commande creerCommande() {
	return new CommandeModifierPositionSommets(this.oldPX, this.oldPY, this.newPX, this.newPY, this.sommets);
    }

}
