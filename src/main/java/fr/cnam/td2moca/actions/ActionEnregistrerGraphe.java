package fr.cnam.td2moca.actions;

import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.io.RedacteurGraphe;
import fr.cnam.td2moca.io.RedacteurGrapheXML;
import fr.cnam.td2moca.model.Graphe;

/**
 * @author Christopher MILAZZO
 */
public class ActionEnregistrerGraphe extends Action {

    private RedacteurGraphe redacteurGraphe;
    private JFileChooser fileChooser;
    private Graphe graphe;

    public ActionEnregistrerGraphe(Invocateur invocateur, Main context) throws IOException {
	super(invocateur, context);
	this.context = context;
	this.fileChooser = new JFileChooser();
	this.fileChooser.setDialogTitle("Enregistrer sous");
	this.fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
	fileChooser.setFileFilter(new FileNameExtensionFilter("*.xml", "xml"));
	this.redacteurGraphe = new RedacteurGrapheXML();
    }

    public Commande creerCommande() {
	this.graphe = this.context.getGraphe();
	if (graphe == null) {
	    return null;
	}
	try {
	    String cheminFichier = this.context.getNomFichierCourant();
	    if (cheminFichier == null) {
		int userSelection = fileChooser.showSaveDialog(this.context.getFrame());

		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    cheminFichier = fileChooser.getSelectedFile().getAbsolutePath();
		    if (!cheminFichier.endsWith(".xml")) {
			cheminFichier += ".xml";
		    }

		    this.context.setNomFichierCourant(cheminFichier);
		} else {
		    return null;
		}
	    }
	    this.redacteurGraphe.enregistrerGraphe(cheminFichier, this.graphe);
	    this.context.setNeedToSave(false);
	} catch (Exception error) {
	    error.printStackTrace();
	    JOptionPane.showMessageDialog(null, "Une erreur s'est produite lors de l'enregistrement du graphe.",
		    "Erreur", JOptionPane.ERROR_MESSAGE);
	}
	return null;
    }
}