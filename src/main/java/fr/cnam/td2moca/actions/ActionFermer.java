package fr.cnam.td2moca.actions;

import java.io.IOException;

import javax.swing.JOptionPane;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.model.Graphe;

public class ActionFermer extends Action {

    private static final String WARNING_MESSAGE_SAVE = "Voulez-vous enregistrer les modifications apportées au graphe ?";
    private static final String ERROR_MESSAGE_SAVE = "Erreur lors de l'enregistrement.";

    private Invocateur invocateur;

    public ActionFermer(Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.invocateur = invocateur;
    }

    @Override
    protected Commande creerCommande() {
	if (this.context.isNeedToSave()) {
	    int res = JOptionPane.showConfirmDialog(null, WARNING_MESSAGE_SAVE, "Attention",
		    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
	    switch (res) {
	    case JOptionPane.YES_OPTION:
		try {
		    new ActionEnregistrerGraphe(this.invocateur, this.context).creerCommande();
		} catch (IOException e) {
		    JOptionPane.showInternalMessageDialog(null, ERROR_MESSAGE_SAVE);
		    return null;
		}
		break;
	    case JOptionPane.NO_OPTION:
		break;
	    case JOptionPane.CLOSED_OPTION:
	    case JOptionPane.CANCEL_OPTION:
	    default:
		return null;
	    }
	}
	this.context.definirGraphe(new Graphe("Untitled"));
	this.context.getPanneauGraphique().toutEffacer();
	this.context.effacerHistorique();
	this.context.setNomFichierCourant(null);
	this.context.setNeedToSave(false);

	return null;
    }

}
