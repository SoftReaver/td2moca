package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeSupprimerSommet;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class ActionSupprimerSommet extends Action {

    private Sommet sommet;

    public ActionSupprimerSommet(Sommet sommetASupprimer, Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.sommet = sommetASupprimer;
    }

    public Commande creerCommande() {
	return new CommandeSupprimerSommet(this.sommet, this.context.getGraphe(), this.context.getPanneauGraphique());
    }
}