package fr.cnam.td2moca.actions;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeAjouterArc;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Sommet;

/**
 * Permet d'ajouter l'arc passé en paramètre du constructeur au graphe courant.
 * Si aucun arc n'est fourni, une popup sera affichée afin que l'utilisateur
 * définisse les données de l'arc.
 * 
 * @author Christopher MILAZZO
 */
public class ActionAjouterArc extends Action {

    private JComboBox<Sommet> listeSommetsOrigine;
    private JComboBox<Sommet> listeSommetsDestination;

    private Arc arc;
    private JDialog frame;

    public ActionAjouterArc(Invocateur invocateur, Main context) {
	this(invocateur, context, null);
    }

    public ActionAjouterArc(Invocateur invocateur, Main context, Arc arcAAjouter) {
	super(invocateur, context);
	this.arc = arcAAjouter;
    }

    public Commande creerCommande() {
	if (this.arc == null) {
	    showWindow();
	}

	if (this.arc == null) {
	    return null;
	} else {
	    Arc arc = this.arc;
	    this.arc = null;
	    return new CommandeAjouterArc(arc, this.context.getGraphe(),
		    ((PanneauGraphique) this.context.getComponentsRefMap().get(Main.PANNEAU_GRAPHIQUE_ID)));
	}
    }

    protected void setArc(Arc arc) {
	this.arc = arc;
    }

    protected JComboBox<Sommet> getSommetsOrigineListe() {
	return this.listeSommetsOrigine;
    }

    protected JComboBox<Sommet> getSommetsDestinationListe() {
	return this.listeSommetsDestination;
    }

    private void showWindow() {
	this.frame = new JDialog(this.context.getFrame(), "Nouvel arc", true);
	FlowLayout layout = new FlowLayout();
	Container container = new Container();
	container.setLayout(layout);

	this.listeSommetsOrigine = new JComboBox<Sommet>();
	this.listeSommetsDestination = new JComboBox<Sommet>();

	for (Sommet sommet : this.context.getGraphe().getListeSommets()) {
	    this.listeSommetsOrigine.addItem(sommet);
	    this.listeSommetsDestination.addItem(sommet);
	}

	JButton okButton = new JButton("OK");
	okButton.addActionListener(new OkAction(this, frame));

	JButton cancelButton = new JButton("Retour");
	cancelButton.addActionListener(new CloseAction(frame));

	container.add(this.listeSommetsOrigine);
	container.add(this.listeSommetsDestination);
	container.add(okButton);
	container.add(cancelButton);

	frame.add(container);
	frame.setBounds(120, 120, 200, 150);
	frame.setResizable(false);
	frame.setVisible(true);
    }

    private class OkAction implements ActionListener {
	private ActionAjouterArc actionAjouterArc;
	private Sommet sommetOrigine;
	private Sommet sommetDestination;
	private JDialog frame;

	public OkAction(ActionAjouterArc action, JDialog frame) {
	    this.actionAjouterArc = action;
	    this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	    this.sommetOrigine = (Sommet) this.actionAjouterArc.getSommetsOrigineListe().getSelectedItem();
	    this.sommetDestination = (Sommet) this.actionAjouterArc.getSommetsDestinationListe().getSelectedItem();
	    Arc arc = new Arc(sommetOrigine, sommetDestination);
	    this.actionAjouterArc.setArc(arc);
	    this.frame.dispose();
	}
    }

    private class CloseAction implements ActionListener {
	private JDialog frame;

	public CloseAction(JDialog frame) {
	    this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	    this.frame.dispose();
	}
    }

}
