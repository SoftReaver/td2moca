package fr.cnam.td2moca.actions;

import javax.swing.JTable;
import javax.swing.table.TableModel;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeModifierArc;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class ActionModifierArcDestination extends Action {

    private int LSColumn;

    public ActionModifierArcDestination(Invocateur invocateur, Main context, int LSColumn) {
	super(invocateur, context);
	this.LSColumn = LSColumn;
    }

    public Commande creerCommande() {
	Integer valeurSommet = null;
	TableModel tableModel = ((JTable) this.context.getComponentsRefMap().get(Main.LISTE_SUCCESSEURS_JTABLE))
		.getModel();
	int ancienSuccesseur = this.context.getLS().get(LSColumn);
	try {
	    valeurSommet = Integer.parseInt(tableModel.getValueAt(0, LSColumn).toString());
	} catch (Exception e2) {
	    tableModel.setValueAt(ancienSuccesseur, 0, LSColumn);
	    return null;
	}
	Sommet nouvelleDestinationSommet = this.context.getGraphe().trouverSommet(Integer.toString(valeurSommet));

	// Si la valeur reste inchangée, on ne fait rien
	if (ancienSuccesseur == valeurSommet.intValue()) {
	    return null;
	}

	if (nouvelleDestinationSommet == null) {
	    tableModel.setValueAt(ancienSuccesseur, 0, LSColumn);
	    return null;
	} else {
	    Graphe graphe = this.context.getGraphe();
	    Sommet sommetOrigine = graphe.trouverSommet(tableModel.getColumnName(LSColumn));
	    Sommet sommetDestination = graphe.trouverSommet(String.valueOf(ancienSuccesseur));
	    Arc ancienArc = this.context.getGraphe().trouverArc(sommetOrigine, sommetDestination);

	    if (ancienArc != null) {
		Sommet nouvelleOrigine;
		// Si le graphe n'est pas orienté, il faut traiter le cas où les sommets
		// d'origine et de destination serait inversés par rapport au model de données.
		if (ancienArc.getOrigine().equals(sommetDestination) && ancienArc.getDestination().equals(sommetOrigine)
			&& !graphe.isOriente()) {
		    nouvelleOrigine = ancienArc.getDestination();
		} else {
		    nouvelleOrigine = ancienArc.getOrigine();
		}

		Arc nouvArc = new Arc(nouvelleOrigine, nouvelleDestinationSommet);
		this.context.getLS().set(LSColumn, valeurSommet);
		return new CommandeModifierArc(ancienArc, nouvArc, this.context.getGraphe(),
			this.context.getPanneauGraphique());
	    } else {
		tableModel.setValueAt(ancienSuccesseur, 0, LSColumn);
	    }
	}
	return null;
    }
}