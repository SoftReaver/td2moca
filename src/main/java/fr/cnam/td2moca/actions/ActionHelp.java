package fr.cnam.td2moca.actions;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JOptionPane;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;

public class ActionHelp extends Action {

    private static final String HELP_WIKI_ADDRESS = "https://gitlab.com/SoftReaver/td2moca/-/wikis/guides/user";

    public ActionHelp(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
	    try {
		Desktop.getDesktop().browse(new URI(HELP_WIKI_ADDRESS));
	    } catch (IOException | URISyntaxException e) {
		displayErrorMessage();
		e.printStackTrace();
	    }
	} else {
	    displayErrorMessage();
	}
	return null;
    }

    private void displayErrorMessage() {
	JOptionPane.showMessageDialog(null,
		"Impossible d'ouvrir un navigateur vers la page d'aide. L'aide se trouve à l'adresse : "
			+ HELP_WIKI_ADDRESS,
		"Erreur", JOptionPane.ERROR_MESSAGE);
    }

}
