package fr.cnam.td2moca.actions;

import java.awt.Color;
import java.util.ArrayList;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.algos.KruskalAlgo;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;

public class ActionKruskalAlgo extends Action {
    
    public ActionKruskalAlgo(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	Graphe graphe = this.context.getGraphe().getPartiel();
	PanneauGraphique panneauGraphique = this.context.getPanneauGraphique();
	ArrayList<Arc> cheminLePlusCourt = new ArrayList<Arc>();
	
	new KruskalAlgo(graphe, cheminLePlusCourt).executer();
	
	panneauGraphique.removeArcsColors();
	for (Arc arc : cheminLePlusCourt) {
	    panneauGraphique.colorShape(arc, Color.RED);
	}
	return null;
    }

}
