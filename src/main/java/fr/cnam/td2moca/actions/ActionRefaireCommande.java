package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;

public class ActionRefaireCommande extends Action {

    private Invocateur invocateur;

    public ActionRefaireCommande(Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.invocateur = invocateur;
    }

    @Override
    protected Commande creerCommande() {
	if (this.invocateur.hasProchaineCommande()) {
	    this.invocateur.refaire();
	}
	return null;
    }
}
