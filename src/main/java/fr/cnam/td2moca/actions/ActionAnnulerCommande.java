package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;

public class ActionAnnulerCommande extends Action {

    private Invocateur invocateur;

    public ActionAnnulerCommande(Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.invocateur = invocateur;
    }

    @Override
    protected Commande creerCommande() {
	if (this.invocateur.hasCommandePrecedente()) {
	    this.invocateur.annuler();
	}
	return null;
    }
}
