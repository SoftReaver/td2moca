package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeAfficherSommet;
import fr.cnam.td2moca.model.Sommet;

public class ActionAfficherSommet extends Action {

    private Sommet sommet;

    public ActionAfficherSommet(Sommet sommet, Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.sommet = sommet;
    }

    @Override
    protected Commande creerCommande() {
	return new CommandeAfficherSommet(this.sommet, this.context.getGraphe(), this.context.getPanneauGraphique());
    }

}
