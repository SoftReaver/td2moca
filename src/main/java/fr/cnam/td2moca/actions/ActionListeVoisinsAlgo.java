package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.algos.ListeVoisinsAlgo;
import fr.cnam.td2moca.commandes.Commande;

public class ActionListeVoisinsAlgo extends Action {

    public ActionListeVoisinsAlgo(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	new ListeVoisinsAlgo(this.context.getGraphe().getPartiel()).executer();
	return null;
    }
}
