package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeAfficherTousLesSommets;

public class ActionAfficherTousLesSommets extends Action {

    public ActionAfficherTousLesSommets(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	return new CommandeAfficherTousLesSommets(this.context.getGraphe(), this.context.getPanneauGraphique());
    }

}
