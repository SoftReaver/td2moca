package fr.cnam.td2moca.actions;

import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.algos.ColoriageAlgo;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class ActionColoriageAlgo extends Action {
    /* Affectation des couleurs par sommet */
    private HashMap<Sommet, Color> mapColoration;

    private Graphe graphe;

    public ActionColoriageAlgo(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	// On s'assure d'avoir le graphe à jour
	this.graphe = this.context.getGraphe().getPartiel();
	// On efface la map précédente pour rendre la classe sans état
	this.mapColoration = new HashMap<Sommet, Color>();
	new ColoriageAlgo(graphe, mapColoration).executer();
	colorierSommets();
	return null;
    }
    
    /**
     * Applique les courleurs aux sommets en utilisant les méthode colorShape() du
     * panneau graphique
     */
    private void colorierSommets() {
	PanneauGraphique panneauGraphique = this.context.getPanneauGraphique();
	panneauGraphique.removeSommetsColors();
	Iterator<Entry<Sommet, Color>> it = this.mapColoration.entrySet().iterator();
	while (it.hasNext()) {
	    Entry<Sommet, Color> entry = it.next();
	    panneauGraphique.colorShape(entry.getKey(), entry.getValue());
	}
    }
}
