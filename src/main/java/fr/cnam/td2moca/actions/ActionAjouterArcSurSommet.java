package fr.cnam.td2moca.actions;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Sommet;

public class ActionAjouterArcSurSommet extends Action {
    private Sommet sommetOrigine;
    private Invocateur invocateur;
    private boolean drawingArc;

    public ActionAjouterArcSurSommet(Invocateur invocateur, Main context, Sommet sommetOrigine) {
	super(invocateur, context);
	this.invocateur = invocateur;
	this.sommetOrigine = sommetOrigine;
	this.drawingArc = true;
    }

    @Override
    protected Commande creerCommande() {
	new AjoutArc(this.context.getPanneauGraphique(), this.sommetOrigine).start();
	return null;
    }

    /**
     * Thread lancé pour l'ajout d'un Arc en dynamique sur le panneau graphique. Une
     * fois lancé il affiche l'arc futur à ajouter. Si un arc de destination est
     * selectionné alors le thread cré une nouvelle commande "AjouterArc" sinon il
     * annule l'action d'ajout de l'arc.
     * 
     * @author Christopher MILAZZO
     *
     */
    private class AjoutArc extends Thread {
	private CustomMouseAdapter mouseAdapter;

	public AjoutArc(PanneauGraphique panneauGraphique, Sommet sommetOrigine) {
	    this.mouseAdapter = new CustomMouseAdapter(panneauGraphique, sommetOrigine);
	}

	@Override
	public void run() {
	    // Accroche d'un mouse listener sur le panneau graphique
	    context.getPanneauGraphique().addMouseListener(this.mouseAdapter);
	    context.getPanneauGraphique().addMouseMotionListener(this.mouseAdapter);

	    while (!interrupted() && drawingArc) {
		try {
		    sleep(100);
		} catch (InterruptedException e) {
		}
	    }

	    // Suppression du mouse listener
	    context.getPanneauGraphique().removeMouseListener(this.mouseAdapter);
	    context.getPanneauGraphique().removeMouseMotionListener(this.mouseAdapter);
	}
    }

    /**
     * Classe d'implementation des comportements aux cliques et glissé-déposés sur
     * les éléments graphiques.
     * 
     * @author Christopher MILAZZO
     *
     */
    private class CustomMouseAdapter extends MouseAdapter {
	private PanneauGraphique panneauGraphique;
	private Sommet sommetDestination;
	private Sommet sommetDestinationTemporaire; // Représente un sommet fictif permettant de suivre le mouvement de
						    // la souris
	private Arc arcTemporaire;

	public CustomMouseAdapter(PanneauGraphique panneauGraphique, Sommet sommetOrigine) {
	    this.panneauGraphique = panneauGraphique;
	    this.sommetDestination = null;
	    this.sommetDestinationTemporaire = new Sommet("temp");
	    this.sommetDestinationTemporaire.x = sommetOrigine.x;
	    this.sommetDestinationTemporaire.y = sommetOrigine.y;

	    this.arcTemporaire = new Arc(sommetOrigine, this.sommetDestinationTemporaire);
	    this.panneauGraphique.draw(this.arcTemporaire);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	    // On efface l'arc puisqu'il sera dessiné par l'action ActionAjouterArc au
	    // besoin.
	    this.panneauGraphique.erase(this.arcTemporaire);

	    this.sommetDestination = panneauGraphique.getSommetCible(e.getPoint());
	    if (this.sommetDestination != null) {
		Arc nouvArc = new Arc(this.arcTemporaire.getOrigine(), sommetDestination);
		new ActionAjouterArc(invocateur, context, nouvArc).perform("Ajout de l'arc "
			+ nouvArc.getOrigine().getLabel() + " -> " + nouvArc.getDestination().getLabel());
	    }
	    drawingArc = false; // On signal au thread que l'opération est terminée.
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	    // Mise à jour de la position du sommet fictif en fonction du mouvement de la
	    // souris.
	    this.sommetDestinationTemporaire.x = e.getPoint().x;
	    this.sommetDestinationTemporaire.y = e.getPoint().y;
	    this.panneauGraphique.sync();
	}
    }
}
