package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeSupprimerArc;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;

/**
 * @author Christopher MILAZZO
 */
public class ActionSupprimerArc extends Action {

    private Arc arc;

    public ActionSupprimerArc(Arc arcASupprimer, Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.arc = arcASupprimer;
    }

    public Commande creerCommande() {
	return new CommandeSupprimerArc(this.arc, this.context.getGraphe(),
		((PanneauGraphique) this.context.getComponentsRefMap().get(Main.PANNEAU_GRAPHIQUE_ID)));
    }
}
