package fr.cnam.td2moca.actions;

import javax.swing.JOptionPane;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.model.Graphe;

public class ActionNouveau extends Action {

    private Invocateur invocateur;

    public ActionNouveau(Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.invocateur = invocateur;
    }

    @Override
    protected Commande creerCommande() {
	new ActionFermer(this.invocateur, this.context).creerCommande();

	if (!this.context.isNeedToSave()) {
	    String nomGraphe = JOptionPane.showInputDialog("Nom du graphe");
	    if (nomGraphe == null) {
		nomGraphe = "Untitled";
	    }
	    this.context.definirGraphe(new Graphe(nomGraphe));
	}

	return null;
    }
}
