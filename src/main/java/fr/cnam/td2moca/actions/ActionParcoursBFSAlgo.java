package fr.cnam.td2moca.actions;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.algos.ParcoursBFSAlgo;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class ActionParcoursBFSAlgo extends Action {
    
    private JComboBox<Sommet> listeSommets;
    private JDialog frame;
    private Sommet sommetDepart;

    public ActionParcoursBFSAlgo(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	this.sommetDepart = null;
	Graphe graphe = this.context.getGraphe().getPartiel();
	
	showWindow();
	
	if (this.sommetDepart == null) {
	    this.sommetDepart = graphe.getListeSommets().get(0);
	}
	
	PanneauGraphique panneauGraphique = this.context.getPanneauGraphique();
	LinkedList<Sommet> sommetsQueue = new LinkedList<Sommet>();
	new ParcoursBFSAlgo(graphe, this.sommetDepart, sommetsQueue).executer();
	
	new Thread(new Runnable() {  
	    @Override
	    public void run() {
		panneauGraphique.removeSommetsColors();
		for (Sommet sommet : sommetsQueue) {
		    panneauGraphique.colorShape(sommet, Color.DARK_GRAY);
		    try {
			Thread.sleep(500);
		    } catch (InterruptedException silent) {}
		}
	    }
	}).start();
	
	JOptionPane.showMessageDialog(null, String.format("<html><body><p style='width: 200px;'>Les sommets ont été parcourcus dans l'ordre suivant :<br> %s</p></body></html>", sommetsQueue));
	return null;
    }
    
    protected void setSommet(Sommet sommet) {
	this.sommetDepart = sommet;
    }
    
    protected JComboBox<Sommet> getListeSommets() {
	return this.listeSommets;
    }
    
    private void showWindow() {
	this.frame = new JDialog(this.context.getFrame(), "Sommet de départ", true);
	FlowLayout layout = new FlowLayout();
	Container container = new Container();
	container.setLayout(layout);

	this.listeSommets = new JComboBox<Sommet>();

	for (Sommet sommet : this.context.getGraphe().getListeSommets()) {
	    this.listeSommets.addItem(sommet);
	}

	JButton okButton = new JButton("OK");
	okButton.addActionListener(new OkAction(this, frame));

	container.add(this.listeSommets);
	container.add(okButton);

	frame.add(container);
	frame.setBounds(120, 120, 200, 150);
	frame.setResizable(false);
	frame.setVisible(true);
    }

    private class OkAction implements ActionListener {
	private ActionParcoursBFSAlgo actionParcoursBFSAlgo;
	private JDialog frame;

	public OkAction(ActionParcoursBFSAlgo action, JDialog frame) {
	    this.actionParcoursBFSAlgo = action;
	    this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	    this.actionParcoursBFSAlgo.setSommet((Sommet) this.actionParcoursBFSAlgo.getListeSommets().getSelectedItem());
	    this.frame.dispose();
	}
    }
}
