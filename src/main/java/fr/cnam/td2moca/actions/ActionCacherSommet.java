package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeCacherSommet;
import fr.cnam.td2moca.model.Sommet;

public class ActionCacherSommet extends Action {

    private Sommet sommet;

    public ActionCacherSommet(Sommet sommet, Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.sommet = sommet;
    }

    @Override
    protected Commande creerCommande() {
	return new CommandeCacherSommet(this.sommet, this.context.getGraphe(), this.context.getPanneauGraphique());
    }

}
