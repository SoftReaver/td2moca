package fr.cnam.td2moca.actions;

import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.io.LecteurGraphe;
import fr.cnam.td2moca.io.LecteurGrapheXML;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class ActionChargerGraphe extends Action {

    private static final String ERROR_MESSAGE = "Une erreur s'est produite lors de la tentative de chargement du graphe.";

    private LecteurGraphe lecteurGraphe;
    private JFileChooser fileChooser;
    private Invocateur invocateur;

    public ActionChargerGraphe(Invocateur invocateur, Main context) {
	super(invocateur, context);
	this.invocateur = invocateur;
	this.fileChooser = new JFileChooser();
	this.fileChooser.setDialogTitle("Chager");
	this.fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
	fileChooser.setFileFilter(new FileNameExtensionFilter("*.xml", "xml"));
	this.lecteurGraphe = new LecteurGrapheXML();
    }

    public Commande creerCommande() {
	try {

	    int userSelection = fileChooser.showOpenDialog(this.context.getFrame());
	    String cheminFichier = null;
	    if (userSelection == JFileChooser.APPROVE_OPTION) {
		new ActionFermer(this.invocateur, this.context).creerCommande();
		cheminFichier = fileChooser.getSelectedFile().getAbsolutePath();

		if (!this.context.isNeedToSave()) {
		    this.context.setNomFichierCourant(cheminFichier);
		    this.context.definirGraphe(this.lecteurGraphe.chargerGraphe(cheminFichier));

		    // Ajouter au panneau graphique les éléments à dessiner
		    PanneauGraphique panneauGraphique = this.context.getPanneauGraphique();
		    ArrayList<Sommet> sommetsADessiner = new ArrayList<Sommet>();
		    for (Sommet sommet : this.context.getGraphe().getListeSommets()) {
			if (sommet.isVisible()) {
			    sommetsADessiner.add(sommet);
			    panneauGraphique.draw(sommet);
			}
		    }

		    for (Arc arc : this.context.getGraphe().getListeArcs()) {
			if (sommetsADessiner.contains(arc.getOrigine())
				&& sommetsADessiner.contains(arc.getDestination())) {
			    panneauGraphique.draw(arc);
			}
		    }
		}
	    }

	} catch (Exception error) {
	    error.printStackTrace();
	    JOptionPane.showMessageDialog(null, ERROR_MESSAGE, "Erreur", JOptionPane.ERROR_MESSAGE);
	}
	return null;
    }
}