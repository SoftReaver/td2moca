package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeDefinirArcsPonderes;
import fr.cnam.td2moca.model.Graphe;

public class ActionDefinirArcsPonderes extends Action {
    public ActionDefinirArcsPonderes(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	Graphe graphe = this.context.getGraphe();
	return new CommandeDefinirArcsPonderes(graphe, !graphe.isPonderationArc());
    }
}
