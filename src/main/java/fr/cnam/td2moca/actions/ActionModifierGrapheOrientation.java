package fr.cnam.td2moca.actions;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.Commande;
import fr.cnam.td2moca.commandes.CommandeChangerOrientationGraphe;
import fr.cnam.td2moca.model.Graphe;

public class ActionModifierGrapheOrientation extends Action {
    public ActionModifierGrapheOrientation(Invocateur invocateur, Main context) {
	super(invocateur, context);
    }

    @Override
    protected Commande creerCommande() {
	Graphe graphe = this.context.getGraphe();
	return new CommandeChangerOrientationGraphe(graphe, !graphe.isOriente());
    }
}
