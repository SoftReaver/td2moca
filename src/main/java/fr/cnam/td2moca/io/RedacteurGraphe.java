package fr.cnam.td2moca.io;

import fr.cnam.td2moca.model.Graphe;

/**
 * 
 */
public interface RedacteurGraphe {

    public void enregistrerGraphe(String cheminFichier, Graphe graphe);

}
