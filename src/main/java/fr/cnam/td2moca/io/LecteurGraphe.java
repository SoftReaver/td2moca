package fr.cnam.td2moca.io;

import fr.cnam.td2moca.model.Graphe;

/**
 * 
 */
public interface LecteurGraphe {

    public Graphe chargerGraphe(String cheminFichier);

}