package fr.cnam.td2moca.io;

import java.io.FileOutputStream;
import java.io.IOException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class RedacteurGrapheXML implements RedacteurGraphe {

    /**
     * @param cheminFichier
     * @return
     */
    @Override
    public void enregistrerGraphe(String cheminFichier, Graphe graphe) {
	try {
	    if (graphe == null) {
		throw new RuntimeException("Aucune donnée à écrire");
	    }
	    XMLOutputter xlmWritter = new XMLOutputter(Format.getPrettyFormat());
	    Element elementRacine = new Element("graphe");
	    elementRacine.setAttribute("nom", graphe.getNom());
	    elementRacine.setAttribute("oriente", (graphe.isOriente()) ? "true" : "false");
	    elementRacine.setAttribute("pondere", (graphe.isPonderationArc()) ? "true" : "false");

	    Document xlmDocument = new Document(elementRacine);

	    // Les sommets
	    Element elementSommets = new Element("sommets");
	    elementRacine.addContent(elementSommets);
	    for (Sommet sommet : graphe.getListeSommets()) {
		Element elementSommet = new Element("sommet");
		elementSommet.setAttribute("label", sommet.getLabel());
		elementSommet.setAttribute("visible", (sommet.isVisible()) ? "true" : "false");

		Element elementPX = new Element("px");
		Element elementPY = new Element("py");
		elementPX.setText(String.valueOf(sommet.x));
		elementPY.setText(String.valueOf(sommet.y));

		elementSommet.addContent(elementPX);
		elementSommet.addContent(elementPY);

		elementSommets.addContent(elementSommet);
	    }

	    // Les arcs
	    Element elementArcs = new Element("arcs");
	    elementRacine.addContent(elementArcs);
	    for (Arc arc : graphe.getListeArcs()) {
		Element elementArc = new Element("arc");
		if (arc.getLabel() != null) {
		    elementArc.setAttribute("label", arc.getLabel());
		}
		elementArc.setAttribute("origine", arc.getOrigine().getLabel());
		elementArc.setAttribute("destination", arc.getDestination().getLabel());
		elementArc.setAttribute("valeur-ponderation", String.valueOf(arc.getValeurPonderation()));
		if (arc.getLabel() != null) {		    
		    elementArc.setAttribute("label", arc.getLabel());
		}

		elementArcs.addContent(elementArc);
	    }

	    FileOutputStream out = new FileOutputStream(cheminFichier);
	    xlmWritter.output(xlmDocument, out);
	    out.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e.getMessage());
	}

    }
}
