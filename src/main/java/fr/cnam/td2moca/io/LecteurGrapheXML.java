package fr.cnam.td2moca.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class LecteurGrapheXML implements LecteurGraphe {

    private static final String DTD_FILE_PATH = "src/main/resources/graphe.dtd";

    /**
     * @param cheminFichier
     * @return
     */
    public Graphe chargerGraphe(String cheminFichier) {
	Graphe graphe = new Graphe("");
	SAXBuilder domBuilder = new SAXBuilder();
	Document document;

	// Parser DTD
	domBuilder.setEntityResolver(new EntityResolver() {

	    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		InputStream in = cl.getResourceAsStream(DTD_FILE_PATH);
		return new InputSource(in);
	    }
	});
	try {
	    document = domBuilder.build(new File(cheminFichier));

	    Element elementGraphe = document.getRootElement();
	    graphe.setNom(elementGraphe.getAttributeValue("nom"));
	    graphe.setOriente((elementGraphe.getAttributeValue("oriente").equals("true")) ? true : false);
	    graphe.setPonderationArc((elementGraphe.getAttributeValue("pondere").equals("true")) ? true : false);

	    // Chargement des sommets
	    Element elementSommets = elementGraphe.getChild("sommets");
	    for (Element elementSommet : elementSommets.getChildren()) {
		Sommet sommet = new Sommet(elementSommet.getAttributeValue("label"));
		sommet.setVisible((elementSommet.getAttributeValue("visible").equals("true")) ? true : false);

		Element elementPosition;
		if ((elementPosition = elementSommet.getChild("px")) != null) {
		    sommet.x = Double.parseDouble(elementPosition.getValue());
		}
		if ((elementPosition = elementSommet.getChild("py")) != null) {
		    sommet.y = Double.parseDouble(elementPosition.getValue());
		}
		graphe.ajouterSommet(sommet);
	    }

	    // Chargement des arcs
	    Element elementArcs = elementGraphe.getChild("arcs");
	    for (Element elementArc : elementArcs.getChildren()) {
		Arc arc = new Arc(graphe.trouverSommet(elementArc.getAttributeValue("origine")),
			graphe.trouverSommet(elementArc.getAttributeValue("destination")));
		Attribute attribute;
		if ((attribute = elementArc.getAttribute("valeur-ponderation")) != null) {
		    arc.setValeurPonderation(attribute.getFloatValue());
		}
		if ((attribute = elementArc.getAttribute("label")) != null) {
		    arc.setLabel(attribute.getValue());
		}
		graphe.ajouterArc(arc);
	    }
	} catch (Exception e) {
	    System.err.println(e.getMessage());
	    throw new RuntimeException();
	}

	return graphe;
    }
}
