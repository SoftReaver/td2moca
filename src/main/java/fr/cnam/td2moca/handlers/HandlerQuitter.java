package fr.cnam.td2moca.handlers;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.actions.ActionQuitter;

public class HandlerQuitter implements WindowListener {
	private Main context;
	private Invocateur invocateur;

	public HandlerQuitter(Invocateur invocateur, Main context) {
	    this.context = context;
	    this.invocateur = invocateur;
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
	    new ActionQuitter(this.invocateur, this.context).perform("Quitter");
	}
}

