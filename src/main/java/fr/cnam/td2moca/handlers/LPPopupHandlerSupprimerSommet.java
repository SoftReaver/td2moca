package fr.cnam.td2moca.handlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JList;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.actions.ActionSupprimerSommet;
import fr.cnam.td2moca.model.Sommet;

public class LPPopupHandlerSupprimerSommet implements ActionListener {
	private Main context;

	public LPPopupHandlerSupprimerSommet(Main context) {
	    this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	    @SuppressWarnings("unchecked")
	    Sommet selectedSommet = ((JList<Sommet>) this.context.getComponentsRefMap().get(Main.LISTE_SOMMETS_JLIST))
		    .getSelectedValue();
	    if (selectedSommet != null) {
		new ActionSupprimerSommet(selectedSommet, this.context.getInvocateur(), this.context)
			.perform("Supprimer sommet");
	    }
	}
}
