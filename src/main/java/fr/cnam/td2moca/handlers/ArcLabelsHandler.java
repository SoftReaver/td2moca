package fr.cnam.td2moca.handlers;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

import fr.cnam.td2moca.Invocateur;
import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.actions.ActionDefinirPonderation;
import fr.cnam.td2moca.model.Arc;

public class ArcLabelsHandler implements KeyListener, FocusListener {
    private String value;
    private Main context;
    private Invocateur invocateur;
    private Arc arc;
    
    public ArcLabelsHandler(Main context, Invocateur invocateur, Arc arc) {
	this.context = context;
	this.invocateur = invocateur;
	this.arc = arc;
    }

    @Override
    public void keyPressed(KeyEvent e) {
	if (e.getKeyCode() == KeyEvent.VK_ENTER) {
	    try {		
		this.value = ((JTextField) e.getSource()).getText();
		new ActionDefinirPonderation(this.invocateur, this.context, this.arc, Float.valueOf(this.value)).perform("Definir pondération");
	    } catch (NumberFormatException silent) {
	    } finally {
		this.context.getPanneauGraphique().requestFocus();
	    }
	} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
	    this.context.getPanneauGraphique().requestFocus();
	}
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void focusGained(FocusEvent e) {
	this.value = ((JTextField) e.getSource()).getText();
	((JTextField) e.getSource()).setText("" + this.arc.getValeurPonderation());
    }

    @Override
    public void focusLost(FocusEvent e) {
    }

}
