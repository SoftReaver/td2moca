package fr.cnam.td2moca.handlers;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.model.Sommet;

public class LPMouseHandler implements MouseListener {
	private Main context;

	public LPMouseHandler(Main context) {
	    this.context = context;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	    if (e.getButton() == MouseEvent.BUTTON3) {
		@SuppressWarnings("unchecked")
		JList<Sommet> list = (JList<Sommet>) e.getSource();
		int row = list.locationToIndex(e.getPoint());
		list.setSelectedIndex(row);
		this.context.showSommetsPopup(e);
	    }
	}
}
