package fr.cnam.td2moca.handlers;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.actions.ActionModifierArcDestination;

public class LSTableModelListenerHandler implements TableModelListener {
    private Main context;

    public LSTableModelListenerHandler(Main context) {
	this.context = context;
    }

    @Override
    public void tableChanged(TableModelEvent e) {
	new ActionModifierArcDestination(this.context.getInvocateur(), this.context, e.getColumn()).perform("Modifier destination arc");
    }
}
