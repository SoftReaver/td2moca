package fr.cnam.td2moca.gui;

import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

public class AboutDialog extends JDialog {

    private static final long serialVersionUID = 1L;
    private JEditorPane textField;

    public AboutDialog() {
	setLocationRelativeTo(null);
	setLayout(new BorderLayout());
	textField = new JEditorPane();
	textField.setContentType("text/html");
	textField.setEditable(false);
	textField.setText(
		"<html><h1>A propos</h1><p>Logiciel de modélisation de graphe libre, développé en java <br>et reposant sur les couche graphique de Swing.</p><p>Ce programme à été développé dans un but pédagogique <br>pour la formation d'ingénieur au CNAM (2021-2022)</p><p>Auteur : Christopher MILAZZO</p><p><a href=\"https://gitlab.com/SoftReaver/td2moca\">Depot GitLab : gitlab.com/SoftReaver/td2moca</p></html>");

	add(new JScrollPane(textField, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
		JScrollPane.HORIZONTAL_SCROLLBAR_NEVER), BorderLayout.CENTER);
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setModalityType(ModalityType.APPLICATION_MODAL);
	pack();
	setVisible(true);
    }
}
