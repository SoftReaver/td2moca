package fr.cnam.td2moca.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.actions.ActionModifierPositionSommets;
import fr.cnam.td2moca.commandes.CommandeModifierPositionSommets;
import fr.cnam.td2moca.handlers.ArcLabelsHandler;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

/**
 * @author Christopher MILAZZO
 */
public class PanneauGraphique extends JPanel {

    private static final long serialVersionUID = 1L;
    private static final int FONT_SIZE = 16;

    private ArrayList<Sommet> sommetsSelectionnes;
    private ArrayList<Sommet> sommetsAffiches;
    private HashMap<Sommet, Color> colorationSommets;
    private ArrayList<Arc> arcsAffiches;
    private HashMap<Arc, Color> colorationArcs;
    private HashMap<Arc, JTextField> arcsLabels;
    private Rectangle selectionBox;

    private Main context;

    public PanneauGraphique(Main context) {
	javax.swing.ToolTipManager.sharedInstance().setDismissDelay(12000);
	javax.swing.ToolTipManager.sharedInstance().setInitialDelay(500);
	this.sommetsAffiches = new ArrayList<Sommet>();
	this.colorationSommets = new HashMap<Sommet, Color>();
	this.arcsAffiches = new ArrayList<Arc>();
	this.colorationArcs = new HashMap<Arc, Color>();
	this.sommetsSelectionnes = new ArrayList<Sommet>();
	this.arcsLabels = new HashMap<Arc, JTextField>();
	MouseAdapter mouseAdapter = new MyMouseAdapter(this);
	this.addMouseListener(mouseAdapter);
	this.addMouseMotionListener(mouseAdapter);
	this.context = context;
    }

    public Main getContext() {
	return this.context;
    }

    public void setSelectionBox(Rectangle selectionBox) {
	this.selectionBox = selectionBox;
    }

    @Override
    protected void paintComponent(Graphics g) {
	super.paintComponent(g);
	Graphics2D g2D = (Graphics2D) g;
	g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	Font font = new Font("Serif", Font.PLAIN, FONT_SIZE - 5);
	g2D.setFont(font);
	g2D.setColor(Color.black);

	Graphe graphe = this.context.getGraphe();
	Graphe graphePartiel = new Graphe(graphe.getNom());

	// On enlève les label qui des arcs qui ne sont plus affichés
	Iterator<Entry<Arc, JTextField>> it = this.arcsLabels.entrySet().iterator();
	while (it.hasNext()) {
	    Entry<Arc, JTextField> entry = it.next();
	    if (!this.arcsAffiches.contains(entry.getKey())) {
		this.remove(entry.getValue());
		it.remove();
	    }
	}

	for (Arc arc : arcsAffiches) {
	    // Synchro des coordonnées de l'arc avec les sommets
	    arc.syncSommets();

	    graphePartiel.ajouterArc(arc);

	    // Cas des arcs colorés
	    if (this.colorationArcs.containsKey(arc)) {
		g2D.setColor(this.colorationArcs.get(arc));
	    } else {
		g2D.setColor(Color.black);
	    }

	    if (this.context.getGraphe().isOriente()) {
		new LineArrow((int) arc.x1, (int) arc.y1, (int) arc.x2, (int) arc.y2, g2D.getColor(), 1).draw(g2D);
	    } else {
		g2D.draw(arc);
	    }

	    // Cas des arcs pondérés
	    if (this.context.getGraphe().isPonderationArc()) { // Affichage des labels
		int deltaX = ((int) arc.getOrigine().x - (int) arc.getDestination().x) / 2;
		int deltaY = ((int) arc.getOrigine().y - (int) arc.getDestination().y) / 2;

		int PX = (int) (arc.getOrigine().x - deltaX);
		int PY = (int) (arc.getOrigine().y - deltaY);

		JTextField textField = null;
		String content = (arc.getLabel() != null) ? arc.getLabel()
			: "" + " (" + arc.getValeurPonderation() + ")";
		if (!arcsLabels.containsKey(arc)) {
		    textField = new JTextField(content.length());
		    textField.addKeyListener(new ArcLabelsHandler(this.context, this.context.getInvocateur(), arc));
		    textField.addFocusListener(new ArcLabelsHandler(this.context, this.context.getInvocateur(), arc));
		    arcsLabels.put(arc, textField);
		    this.add(arcsLabels.get(arc));
		} else {
		    textField = arcsLabels.get(arc);
		}
		if (!textField.hasFocus()) {
		    textField.setText(content);
		}
		textField.setBounds(PX, PY, content.length() * 8, 20);
	    } else {
		// On efface tous les labels
		for (JTextField textField : this.arcsLabels.values()) {
		    this.remove(textField);
		}
		this.arcsLabels.clear();
	    }
	}

	for (Sommet sommet : sommetsAffiches) {
	    graphePartiel.ajouterSommet(sommet);

	    if (this.sommetsSelectionnes.contains(sommet)) {
		g2D.setColor(Color.red);
	    } else {
		g2D.setColor(Color.black);
	    }
	    g2D.draw(sommet);

	    if (this.colorationSommets.containsKey(sommet)) {
		g2D.setColor(this.colorationSommets.get(sommet));
	    } else {
		g2D.setColor(Color.white);
	    }
	    g2D.fill(sommet);

	    if (this.sommetsSelectionnes.contains(sommet)) {
		g2D.setColor(Color.red);
	    } else {
		if (this.colorationSommets.containsKey(sommet)) {
		    g2D.setColor(Color.white);
		} else {
		    g2D.setColor(Color.black);
		}
	    }
	    g2D.drawString(sommet.getLabel(), ((int) sommet.x) + (Sommet.RADIUS / 2) - 5,
		    ((int) sommet.y) + (Sommet.RADIUS / 2) + 5);
	}

	g2D.setColor(Color.black);
	// On affiche des infos sur le graphe global
	g2D.drawString("Nom : " + graphe.getNom(), 10, 20);
	g2D.drawString("Graphe : " + (graphe.isComplet() ? "complet" : "non complet") + ", "
		+ (graphe.isOriente() ? "orienté" : "non-orienté") + ", "
		+ (graphe.isBiParti() ? "biparti" : "non-biparti"), 10, 40);
	g2D.drawString("Ordre : " + graphe.getOrdre(), 10, 60);
	g2D.drawString("Taille : " + graphe.getTaille(), 10, 80);

	// On affiche des infos sur le graphe partiel
	if (graphe.getOrdre() > graphePartiel.getOrdre()) {
	    g2D.drawString("Graphe partiel : ", 10, 120);
	    g2D.drawString("Graphe : " + (graphePartiel.isComplet() ? "complet" : "non complet") + ", "
		    + (graphePartiel.isOriente() ? "orienté" : "non-orienté") + ", "
		    + (graphePartiel.isBiParti() ? "biparti" : "non-biparti"), 10, 140);
	    g2D.drawString("Ordre : " + graphePartiel.getOrdre(), 10, 160);
	    g2D.drawString("Taille : " + graphePartiel.getTaille(), 10, 180);
	}

	// Dessiner le rectangle de selection
	if (this.selectionBox != null) {
	    this.selectionBox.draw(g2D);
	}
    }

    public void draw(Shape shapeToDraw) {
	if (shapeToDraw instanceof Sommet) {
	    if (!this.sommetsAffiches.contains((Sommet) shapeToDraw)) {
		this.sommetsAffiches.add((Sommet) shapeToDraw);
	    }
	} else if (shapeToDraw instanceof Arc) {
	    if (!this.arcsAffiches.contains((Arc) shapeToDraw)) {
		this.arcsAffiches.add((Arc) shapeToDraw);
	    }
	} else {
	    return;
	}
	repaint();
    }

    /**
     * Colorie la forme donnée et redessine le panneau graphique
     * 
     * @param shapeToColor
     */
    public void colorShape(Shape shapeToColor, Color color) {
	if (shapeToColor instanceof Sommet) {
	    if (!this.colorationSommets.containsKey((Sommet) shapeToColor)) {
		this.colorationSommets.put((Sommet) shapeToColor, color);
	    }
	} else if (shapeToColor instanceof Arc) {
	    if (!this.colorationArcs.containsKey((Arc) shapeToColor)) {
		this.colorationArcs.put((Arc) shapeToColor, color);
	    }
	} else {
	    return;
	}
	repaint();
    }

    /**
     * Supprimme toutes les couleurs pour les formes et redessine le
     * panneauGraphique
     */
    public void removeShapesColors() {
	removeSommetsColors();
	removeArcsColors();
    }

    public void removeSommetsColors() {
	this.colorationSommets.clear();
	repaint();
    }

    public void removeArcsColors() {
	this.colorationArcs.clear();
	repaint();
    }

    public void erase(Shape shapeToErase) {
	if (shapeToErase instanceof Sommet) {
	    this.sommetsAffiches.remove((Sommet) shapeToErase);
	    this.colorationSommets.remove(shapeToErase);
	} else if (shapeToErase instanceof Arc) {
	    this.arcsAffiches.remove((Arc) shapeToErase);
	    this.colorationArcs.remove(shapeToErase);
	} else {
	    return;
	}
	repaint();
    }

    public Sommet getSommetCible(Point point) {
	Sommet sommetCible = null;
	for (Sommet sommet : sommetsAffiches) {
	    if (sommet.contains(point)) {
		sommetCible = sommet;
	    }
	}
	return sommetCible;
    }

    public void toutEffacer() {
	this.removeShapesColors();
	this.sommetsAffiches.clear();
	this.arcsAffiches.clear();
	for (JTextField textField : this.arcsLabels.values()) {
	    this.remove(textField);
	}
	this.arcsLabels.clear();
	repaint();
    }

    public void sync() {
	this.repaint();
    }

    /**
     * Classe d'implementation des comportements aux cliques et glissé-déposés sur
     * les éléments graphiques.
     * 
     * @author Christopher MILAZZO
     *
     */
    private class MyMouseAdapter extends MouseAdapter {
	private PanneauGraphique panneauGraphique;
	/* Action glissé-déposé sur sommets */
	private boolean pressed;

	/* Action créer un rectangle de selection */
	private boolean selecting;

	private Rectangle selectionBox;

	private Point point;
	private ArrayList<Sommet> sommets;
	private int oldPX;
	private int oldPY;
	private int newPX;
	private int newPY;
	private int selBoxBegX;
	private int selBoxBegY;

	public MyMouseAdapter(PanneauGraphique panneauGraphique) {
	    this.pressed = false;
	    this.panneauGraphique = panneauGraphique;
	    this.sommets = new ArrayList<Sommet>();
	    this.selectionBox = new Rectangle(0, 0, 0, 0);
	}

	@Override
	public void mousePressed(MouseEvent e) {
	    if (e.getButton() == MouseEvent.BUTTON1) {
		Sommet sommetCible = getSommetCible(e.getPoint());
		if (sommetCible != null) {
		    this.pressed = true;
		    this.point = e.getPoint();
		    this.oldPX = (int) sommetCible.x;
		    this.oldPY = (int) sommetCible.y;
		    this.newPX = this.oldPX;
		    this.newPY = this.oldPY;
		}
		if (this.pressed) {
		    this.sommets.clear();
		    if (sommetsSelectionnes.contains(sommetCible)) {
			this.sommets = new ArrayList<Sommet>(sommetsSelectionnes);
		    } else {
			this.sommets.add(sommetCible);
			sommetsSelectionnes.clear();
			sommetsSelectionnes.add(sommetCible);
		    }
		} else {
		    this.point = e.getPoint();
		    this.selBoxBegX = this.point.x;
		    this.selBoxBegY = this.point.y;
		    this.selecting = true;
		    sommetsSelectionnes.clear();
		    this.sommets.clear();
		    this.panneauGraphique.setSelectionBox(selectionBox);
		}
	    } else if (e.getButton() == MouseEvent.BUTTON3) {
		Sommet sommetCible = getSommetCible(e.getPoint());
		this.panneauGraphique.getContext().showPopup(e, sommetCible);
	    } else {
		return;
	    }
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	    if (this.pressed) {
		int deltaX = e.getX() - point.x;
		int deltaY = e.getY() - point.y;

		this.newPX += deltaX;
		this.newPY += deltaY;

		// Déplacer tous les sommets selectionnés en fonction du mouvement de la souris
		for (Sommet sommet : sommetsSelectionnes) {
		    sommet.x += deltaX;
		    sommet.y += deltaY;
		}

		point = e.getPoint();
	    } else if (this.selecting) {
		point = e.getPoint();

		this.selectionBox.x1 = selBoxBegX;
		this.selectionBox.y1 = selBoxBegY;
		this.selectionBox.x2 = point.x;
		this.selectionBox.y2 = point.y;

		// Détecter les sommets contenu dans la zone de selection
		for (Sommet sommet : sommetsAffiches) {
		    int left, right, up, down;
		    if (this.selectionBox.x1 > this.selectionBox.x2) {
			left = this.selectionBox.x2;
			right = this.selectionBox.x1;
		    } else {
			left = this.selectionBox.x1;
			right = this.selectionBox.x2;
		    }
		    if (this.selectionBox.y1 > this.selectionBox.y2) {
			up = this.selectionBox.y2;
			down = this.selectionBox.y1;
		    } else {
			up = this.selectionBox.y1;
			down = this.selectionBox.y2;
		    }
		    if ((sommet.x >= left && sommet.x <= right) && (sommet.y >= up && sommet.y <= down)) {
			if (!sommetsSelectionnes.contains(sommet)) {
			    sommetsSelectionnes.add(sommet);
			}
		    } else {
			sommetsSelectionnes.remove(sommet);
		    }
		}
	    }
	    this.panneauGraphique.sync();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	    boolean check = false;
	    for (Sommet sommet : sommetsAffiches) {
		if (sommet.contains(e.getPoint())) {
		    setCursor(new Cursor(Cursor.HAND_CURSOR));
		    check = true;
		    Graphe graphe = this.panneauGraphique.context.getGraphe();

		    if (graphe.isOriente()) {
			ArrayList<Sommet> successeurs = graphe.getSuccesseurs(sommet);
			ArrayList<Sommet> predecesseurs = graphe.getPredecesseurs(sommet);
			int nbSuccesseurs = successeurs.size();
			int nbPredecesseurs = predecesseurs.size();

			this.panneauGraphique
				.setToolTipText("<html><p>Sommet " + sommet.getLabel() + "</p><p>" + nbSuccesseurs
					+ " Γ+ | " + nbPredecesseurs + " Γ-</p>"
					+ ((nbSuccesseurs > 0) ? "<p>successeurs : " + successeurs.stream()
						.map(Sommet::getLabel).collect(Collectors.joining(", ")) + "</p>" : "")
					+ ((nbPredecesseurs > 0)
						? "<p>prédécesseurs : " + predecesseurs.stream().map(Sommet::getLabel)
							.collect(Collectors.joining(", ")) + "</p>"
						: "")
					+ "</html>");
		    } else {
			ArrayList<Sommet> voisins = graphe.getSommetsAdjacents(sommet);
			int nbVoisins = voisins.size();
			this.panneauGraphique
				.setToolTipText("<html><p>Sommet " + sommet.getLabel() + "</p><p>" + nbVoisins
					+ " Γ</p>" + ((nbVoisins > 0) ? "<p>adjacents : " + voisins.stream()
						.map(Sommet::getLabel).collect(Collectors.joining(", ")) + "</p>" : "")
					+ "</html>");
		    }
		    break;
		}
	    }
	    if (!check) {
		this.panneauGraphique.setToolTipText(null);
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	    }
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	    int deltaX = this.oldPX - this.newPX;
	    int deltaY = this.oldPY - this.newPY;
	    if (this.pressed && !(deltaX > -1 && deltaX < 1) && !(deltaY > -1 && deltaY < 1)) {
		new CommandeModifierPositionSommets(this.newPX, this.newPY, this.oldPX, this.oldPY, this.sommets)
			.executer();
		// On garde l'historique de l'action de déplacement
		new ActionModifierPositionSommets(this.oldPX, this.oldPY, this.newPX, this.newPY, this.sommets,
			this.panneauGraphique.getContext().getInvocateur(), this.panneauGraphique.getContext())
				.perform("Modifier position sommet");
	    }
	    if (this.selecting) {
		this.panneauGraphique.setSelectionBox(null);
		this.panneauGraphique.sync();
	    }
	    this.pressed = false;
	    this.selecting = false;
	}
    }

    /**
     * Représentation d'un rectangle
     */
    private static class Rectangle {
	public int x1;
	public int y1;
	public int x2;
	public int y2;

	public Rectangle(int x1, int y1, int x2, int y2) {
	    this.x1 = x1;
	    this.y1 = y1;
	    this.x2 = x2;
	    this.y2 = y2;
	}

	public void draw(Graphics g) {
	    if (x1 <= x2 && y1 <= y2) {
		g.drawRect(x1, y1, x2 - x1, y2 - y1);
	    } else if (x1 <= x2 && y1 > y2) {
		g.drawRect(x1, y2, x2 - x1, y1 - y2);
	    } else if (x1 > x2 && y1 <= y2) {
		g.drawRect(x2, y1, x1 - x2, y2 - y1);
	    } else {
		g.drawRect(x2, y2, x1 - x2, y1 - y2);
	    }
	}
    }

    /**
     * Représentation d'une flèche
     */

    private static final Polygon ARROW_HEAD = new Polygon();

    static {
	ARROW_HEAD.addPoint(0, 0);
	ARROW_HEAD.addPoint(-5, -Sommet.RADIUS);
	ARROW_HEAD.addPoint(5, -Sommet.RADIUS);
    }

    private static class LineArrow {

	private final int x;
	private final int y;
	private final int endX;
	private final int endY;
	private final Color color;
	private final int thickness;
	private final double angle;

	public LineArrow(int x, int y, int x2, int y2, Color color, int thickness) {
	    super();
	    this.x = x;
	    this.y = y;
	    this.endX = x2;
	    this.endY = y2;
	    this.color = color;
	    this.thickness = thickness;
	    this.angle = Math.atan2(endY - y, endX - x);
	}

	public void draw(Graphics g) {
	    Graphics2D g2 = (Graphics2D) g;

	    g2.setColor(color);
	    g2.setStroke(new BasicStroke(thickness));
	    g2.drawLine(x, y, (int) (endX - ((Sommet.RADIUS / 2) * Math.cos(angle))),
		    (int) (endY - ((Sommet.RADIUS / 2) * Math.sin(angle))));

	    AffineTransform tx1 = g2.getTransform();
	    AffineTransform tx2 = (AffineTransform) tx1.clone();

	    tx2.translate(endX, endY);
	    tx2.rotate(angle - Math.PI / 2);

	    g2.setTransform(tx2);
	    g2.fill(ARROW_HEAD);

	    g2.setTransform(tx1);
	}
    }
}
