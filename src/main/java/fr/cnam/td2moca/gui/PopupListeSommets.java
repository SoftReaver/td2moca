package fr.cnam.td2moca.gui;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.handlers.LPPopupHandlerAfficherSommet;
import fr.cnam.td2moca.handlers.LPPopupHandlerCacherSommet;
import fr.cnam.td2moca.handlers.LPPopupHandlerSupprimerSommet;

public class PopupListeSommets extends JPopupMenu {
    private static final long serialVersionUID = 1L;

    public PopupListeSommets(Main context) {
	JMenuItem mnItemSupprimerSommet = new JMenuItem("Supprimer sommet");
	mnItemSupprimerSommet.addActionListener(new LPPopupHandlerSupprimerSommet(context));
	this.add(mnItemSupprimerSommet);

	this.addSeparator();

	JMenuItem mnItemAfficherSommet = new JMenuItem("Afficher sommet");
	this.add(mnItemAfficherSommet);
	mnItemAfficherSommet.addActionListener(new LPPopupHandlerAfficherSommet(context));

	JMenuItem mnItemCacherSommet = new JMenuItem("Cacher sommet");
	this.add(mnItemCacherSommet);
	mnItemCacherSommet.addActionListener(new LPPopupHandlerCacherSommet(context));
    }
    
}
