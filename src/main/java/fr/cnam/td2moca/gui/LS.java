package fr.cnam.td2moca.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import fr.cnam.td2moca.Main;

public class LS extends JTable {
    private static final long serialVersionUID = 1L;

    public LS(Main context) {
	this.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	this.setFillsViewportHeight(true);

	JScrollPane scrollPane = new JScrollPane(this);
	scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
	scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
	GridBagConstraints gbc_scrollPane = new GridBagConstraints();
	gbc_scrollPane.fill = GridBagConstraints.BOTH;
	gbc_scrollPane.gridx = 1;
	gbc_scrollPane.gridy = 0;
	this.getTableHeader().setPreferredSize(new Dimension(0, 10));
	this.getTableHeader().setReorderingAllowed(false);
	scrollPane.setPreferredSize(new Dimension(0, 10));
	
	context.getFrame().getContentPane().add(scrollPane, gbc_scrollPane);
    }
}
