package fr.cnam.td2moca.gui;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import fr.cnam.td2moca.Main;

public class PopupGraphicPanel extends JPopupMenu {
    private static final long serialVersionUID = 1L;

    public PopupGraphicPanel(Main context) {
	JMenuItem mnItemAjoutSommet = new JMenuItem("Ajouter sommet");
	this.add(mnItemAjoutSommet);
	mnItemAjoutSommet.addActionListener(context.actionAjouterSommet);

	JMenuItem mnItemAjoutArc = new JMenuItem("Ajouter arc");
	this.add(mnItemAjoutArc);
	mnItemAjoutArc.addActionListener(context.actionAjouterArc);
	context.getComponentsRefMap().put(Main.MENU_ITEM_AJOUTER_ARC_JMENUITEM, mnItemAjoutArc);

	this.addSeparator();

	JMenuItem mnItemAfficherTousSommets = new JMenuItem("Tout afficher");
	this.add(mnItemAfficherTousSommets);
	mnItemAfficherTousSommets.addActionListener(context.actionAfficherTousLesSommets);

	JMenuItem mnItemCacherTousSommets = new JMenuItem("Tout cacher");
	this.add(mnItemCacherTousSommets);
	mnItemCacherTousSommets.addActionListener(context.actionCacherTousLesSommets);
    }
}
