package fr.cnam.td2moca.gui;

import java.awt.event.KeyEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import fr.cnam.td2moca.Main;

public class MenuBar extends JMenuBar {
    private static final long serialVersionUID = 1L;

    public MenuBar(Main context) {
	/* ====================  FICHIER  ================================================================================================= */
	JMenuBar menuBar = new JMenuBar();
	context.getFrame().setJMenuBar(menuBar);

	JMenu mnFichier = new JMenu("Fichier");
	mnFichier.setMnemonic(KeyEvent.VK_F);
	menuBar.add(mnFichier);

	JMenuItem mntmNouveau = new JMenuItem("Nouveau");
	mnFichier.add(mntmNouveau);
	mntmNouveau.addActionListener(context.actionNouveau);
	mntmNouveau.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));

	JMenuItem mntmOuvrir = new JMenuItem("Ouvrir");
	mnFichier.add(mntmOuvrir);
	mntmOuvrir.addActionListener(context.actionChargerGraphe);
	mntmOuvrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));

	JMenuItem mntmEnregistrer = new JMenuItem("Enregistrer");
	mntmEnregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
	mnFichier.add(mntmEnregistrer);
	mntmEnregistrer.addActionListener(context.actionEnregistrerGraphe);

	JMenuItem mntmEnregistrerSous = new JMenuItem("Enregistrer sous");
	mnFichier.add(mntmEnregistrerSous);
	mntmEnregistrerSous.addActionListener(context.actionEnregistrerGrapheSous);

	JMenuItem mntmFermer = new JMenuItem("Fermer");
	mnFichier.add(mntmFermer);
	mntmFermer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_DOWN_MASK));
	mntmFermer.addActionListener(context.actionFermer);

	JMenuItem mntmQuitter = new JMenuItem("Quitter");
	mnFichier.add(mntmQuitter);
	mntmQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK));
	mntmQuitter.addActionListener(context.actionQuitter);
	
	/* ====================  EDIT  ================================================================================================= */

	JMenu mnEdit = new JMenu("Edit");
	mnEdit.setMnemonic(KeyEvent.VK_E);
	menuBar.add(mnEdit);

	JMenuItem mntAnnuler = new JMenuItem("Annuler");
	mnEdit.add(mntAnnuler);
	mntAnnuler.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK));
	mntAnnuler.addActionListener(context.actionAnnulerCommande);
	mntAnnuler.setEnabled(false);
	context.getComponentsRefMap().put(Main.MENU_ITEM_ANNULER_JMENUITEM, mntAnnuler);

	JMenuItem mntRefaire = new JMenuItem("Refaire");
	mnEdit.add(mntRefaire);
	mntRefaire.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, KeyEvent.CTRL_DOWN_MASK));
	mntRefaire.addActionListener(context.actionRefaireCommande);
	mntRefaire.setEnabled(false);
	context.getComponentsRefMap().put(Main.MENU_ITEM_REFAIRE_JMENUITEM, mntRefaire);
	
	/* ====================  GRAPHE  ================================================================================================= */

	JMenu mnGraphe = new JMenu("Graphe");
	mnGraphe.setMnemonic(KeyEvent.VK_G);
	menuBar.add(mnGraphe);

	JMenuItem mntmAjouterSommet = new JMenuItem("Ajouter sommet");
	mnGraphe.add(mntmAjouterSommet);
	mntmAjouterSommet.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, KeyEvent.CTRL_DOWN_MASK));
	mntmAjouterSommet.addActionListener(context.actionAjouterSommet);

	JMenu mnSuppimerSommet = new JMenu("Suppimer sommet");
	mnGraphe.add(mnSuppimerSommet);
	context.getComponentsRefMap().put(Main.MENU_SUPP_SOMMETS_JMENU, mnSuppimerSommet);

	JMenuItem mntmAjouterArc = new JMenuItem("Ajouter arc");
	mnGraphe.add(mntmAjouterArc);
	mntmAjouterArc.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK));
	mntmAjouterArc.addActionListener(context.actionAjouterArc);

	JMenu mnSuppimerArc = new JMenu("Suppimer arc");
	mnGraphe.add(mnSuppimerArc);
	context.getComponentsRefMap().put(Main.MENU_SUPP_ARCS_JMENU, mnSuppimerArc);

	JMenuItem mntmAfficherTousLesSommets = new JMenuItem("Afficher tous les sommets");
	mnGraphe.add(mntmAfficherTousLesSommets);
	mntmAfficherTousLesSommets.addActionListener(context.actionAfficherTousLesSommets);

	JMenuItem mntmCacherTousLesSommets = new JMenuItem("Cacher tous les sommets");
	mnGraphe.add(mntmCacherTousLesSommets);
	mntmCacherTousLesSommets.addActionListener(context.actionCacherTousLesSommets);

	mnGraphe.addSeparator();

	JCheckBoxMenuItem mnCheckboxItemGrapheOriente = new JCheckBoxMenuItem("Graphe orienté");
	mnGraphe.add(mnCheckboxItemGrapheOriente);
	context.getComponentsRefMap().put(Main.MENU_CHECKBOX_ORIENTE_JCHECKBOXMENUITEM, mnCheckboxItemGrapheOriente);
	mnCheckboxItemGrapheOriente.addActionListener(context.actionModifierGrapheOrientation);

	JCheckBoxMenuItem mnCheckboxItemArcsPonderes = new JCheckBoxMenuItem("Arcs pondérés");
	mnGraphe.add(mnCheckboxItemArcsPonderes);
	context.getComponentsRefMap().put(Main.MENU_CHECKBOX_PONDERE_JCHECKBOXMENUITEM, mnCheckboxItemArcsPonderes);
	mnCheckboxItemArcsPonderes.addActionListener(context.actionDefinirArcsPonderes);

	JMenuItem mnItemRenommerGraphe = new JMenuItem("Renommer graphe");
	mnGraphe.add(mnItemRenommerGraphe);
	mnItemRenommerGraphe.addActionListener(context.actionRenommerGraphe);

	mnGraphe.addSeparator();

	JMenu mnExecuter = new JMenu("Executer");
	mnGraphe.add(mnExecuter);
	
	JMenuItem mnItemAfficherVoisins = new JMenuItem("Afficher tous les voisins");
	mnExecuter.add(mnItemAfficherVoisins);
	mnItemAfficherVoisins.addActionListener(context.actionListeVoisinsAlgo);
	
	JMenuItem mnItemColorationSommets = new JMenuItem("Coloration des sommets");
	mnExecuter.add(mnItemColorationSommets);
	mnItemColorationSommets.addActionListener(context.actionColoriageAlgo);
	
	JMenuItem mnItemParcoursBFS = new JMenuItem("Parcours des sommets BFS (largeur)");
	mnExecuter.add(mnItemParcoursBFS);
	mnItemParcoursBFS.addActionListener(context.actionParcoursBFSAlgo);
	
	JMenuItem mnItemParcoursDFS = new JMenuItem("Parcours des sommets DFS (profondeur)");
	mnExecuter.add(mnItemParcoursDFS);
	mnItemParcoursDFS.addActionListener(context.actionParcoursDFSAlgo);
	
	JMenuItem mnItemKruskal = new JMenuItem("Chemin le plus court (Kruskal)");
	mnExecuter.add(mnItemKruskal);
	mnItemKruskal.addActionListener(context.actionKruskalAlgo);
	
	
	/* ====================  AIDE  ================================================================================================= */

	JMenu mnAide = new JMenu("Aide");
	mnAide.setMnemonic(KeyEvent.VK_A);
	menuBar.add(mnAide);

	JMenuItem mntmAPropos = new JMenuItem("A propos");
	mnAide.add(mntmAPropos);
	mntmAPropos.addActionListener(context.actionAbout);

	JMenuItem mntmAide = new JMenuItem("Aide");
	mnAide.add(mntmAide);
	mntmAide.addActionListener(context.actionHelp);
    }
    
}
