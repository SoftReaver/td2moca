package fr.cnam.td2moca.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.handlers.LPMouseHandler;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class LP extends JList<Sommet> {
    private static final long serialVersionUID = 1L;
    private Main context;

    public LP(Main context) {
	this.context = context;
	GridBagConstraints gbc_list = new GridBagConstraints();
	gbc_list.gridheight = 2;
	gbc_list.insets = new Insets(0, 0, 0, 5);
	gbc_list.fill = GridBagConstraints.BOTH;
	gbc_list.gridx = 0;
	gbc_list.gridy = 0;

	DefaultListModel<Sommet> listModel = new DefaultListModel<Sommet>();
	this.setModel(listModel);
	this.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);

	this.setCellRenderer(new SommetRenderer());

	this.addMouseListener(new LPMouseHandler(context));
	context.getFrame().getContentPane().add(new JScrollPane(this), gbc_list);
    }

    private class SommetRenderer extends JLabel implements ListCellRenderer<Sommet> {
	private static final long serialVersionUID = 1L;

	public SommetRenderer() {
	    this.setOpaque(true);
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Sommet> list, Sommet sommet, int index,
		boolean isSelected, boolean cellHasFocus) {

	    Graphe graphe = context.getGraphe();
	    int nbSuccesseurs = graphe.getSuccesseurs(sommet).size();
	    int nbConnectes = graphe.getSommetsAdjacents(sommet).size();
	    
	    if (graphe.isOriente()) {		
		setText(sommet + " | " + nbSuccesseurs + " Γ+ | " + (nbConnectes - nbSuccesseurs) + " Γ-");
	    } else {
		setText(sommet + " | " + nbConnectes + " Γ");
	    }

	    if (isSelected) {
		this.setBackground(list.getSelectionBackground());
		this.setForeground(list.getSelectionForeground());
	    } else if (!sommet.isVisible()) {
		this.setBackground(list.getBackground());
		this.setForeground(Color.RED);
	    } else {
		this.setBackground(list.getBackground());
		this.setForeground(list.getForeground());
	    }

	    return this;
	}

    }
}
