package fr.cnam.td2moca.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Christopher MILAZZO
 */
public class Graphe {

    private String nom;
    private boolean oriente;
    private boolean ponderationArc;
    private ArrayList<Arc> listeArcs;
    private ArrayList<Sommet> listeSommets;
    private HashMap<Sommet, Integer> mapColoration;

    public Graphe(String nom) {
	this.setNom(nom);
	this.oriente = false;
	this.listeSommets = new ArrayList<Sommet>();
	this.listeArcs = new ArrayList<Arc>();
    }

    public void ajouterSommet(Sommet sommet) {
	this.listeSommets.add(sommet);
    }

    public void supprimerSommet(Sommet sommetASupp) {
	this.listeSommets.remove(sommetASupp);
    }

    public void renomerSommet(Sommet sommet, String nouvNom) {
	int index = this.listeSommets.indexOf(sommet);
	if (index >= 0) {
	    this.listeSommets.get(index).setLabel(nouvNom);
	}
	return;
    }

    /**
     * Retourne le sommet portant le nom passé en paramètre. Si aucun sommet trouvé,
     * la valeur null est retournée.
     * 
     * @param nomSommet
     * @return le sommet trouvé ou null
     */
    public Sommet trouverSommet(String nomSommet) {
	Sommet sommetTrouve = null;
	for (Sommet sommet : this.listeSommets) {
	    if (sommet.getLabel().equals(nomSommet)) {
		sommetTrouve = sommet;
		break;
	    }
	}
	return sommetTrouve;
    }

    /**
     * Retourne l'arc faisant la liaison entre les sommets passés en paramètres.
     * Retourne null si aucun arc n'a été trouvé.
     * 
     * @param sommetOrigine
     * @param sommetDestination
     * @return L'arc trouvé ou null
     */
    public Arc trouverArc(Sommet sommetOrigine, Sommet sommetDestination) {
	Arc arcTrouve = null;
	for (Arc arc : listeArcs) {
	    if ((arc.getOrigine().equals(sommetOrigine) && arc.getDestination().equals(sommetDestination))
		    || (!this.oriente && arc.getOrigine().equals(sommetDestination)
			    && arc.getDestination().equals(sommetOrigine))) {
		arcTrouve = arc;
		break;
	    }
	}
	return arcTrouve;
    }

    /**
     * Récupère les successeurs du sommet donné. Si le graphe n'est pas orienté, cette fonction
     * renvoi tous les sommets connectés (voisins) au sommet donné.
     * 
     * @param sommet
     * @return
     */
    public ArrayList<Sommet> getSuccesseurs(Sommet sommet) {
	ArrayList<Sommet> successeurs = new ArrayList<Sommet>();
	for (Arc arc : listeArcs) {
	    if (arc.getOrigine().equals(sommet) && !successeurs.contains(arc.getDestination())) {
		successeurs.add(arc.getDestination());
	    }
	    // On verifie également dans l'autre sens pour les graphes non-orientés
	    if (!this.oriente && arc.getDestination().equals(sommet) && !successeurs.contains(arc.getOrigine())) {
		successeurs.add(arc.getOrigine());
	    }
	}
	return successeurs;
    }
    
    /**
     * Récupère les prédécesseurs du sommet donné. Si le graphe n'est pas orienté, cette fonction
     * renvoi tous les sommets connectés (voisins) au sommet donné.
     * 
     * @param sommet
     * @return
     */
    public ArrayList<Sommet> getPredecesseurs(Sommet sommet) {
	ArrayList<Sommet> predecesseurs = new ArrayList<Sommet>();
	for (Arc arc : listeArcs) {
	    if (arc.getDestination().equals(sommet) && !predecesseurs.contains(arc.getOrigine())) {
		predecesseurs.add(arc.getOrigine());
	    }
	    // On verifie également dans l'autre sens pour les graphes non-orientés
	    if (!this.oriente && arc.getOrigine().equals(sommet) && !predecesseurs.contains(arc.getDestination())) {
		predecesseurs.add(arc.getDestination());
	    }
	}
	return predecesseurs;
    }

    /**
     * Récupère tous les sommets adjacents (voisins) au sommet donné.
     * 
     * @param sommet
     * @return
     */
    public ArrayList<Sommet> getSommetsAdjacents(Sommet sommet) {
	ArrayList<Sommet> sommets = new ArrayList<Sommet>();
	for (Arc arc : listeArcs) {
	    if (arc.getOrigine().equals(sommet) && !sommets.contains(arc.getDestination())) {
		sommets.add(arc.getDestination());
	    } else if (arc.getDestination().equals(sommet) && !sommets.contains(arc.getOrigine())) {
		sommets.add(arc.getOrigine());
	    }
	}
	return sommets;
    }
    
    public boolean areSommetsConnectes(Sommet s1, Sommet s2) {
	ArrayList<Sommet> sommetsMarques = new ArrayList<Sommet>();
	ajouterSommetsAdjacents(s1, sommetsMarques);
	return sommetsMarques.contains(s2); 
    }
    
    private void ajouterSommetsAdjacents(Sommet sommetCourant, ArrayList<Sommet> listeSommets) {
	for (Sommet sommet : this.getSommetsAdjacents(sommetCourant)) {
	    if (!listeSommets.contains(sommet) ) {
		listeSommets.add(sommet);
		ajouterSommetsAdjacents(sommet, listeSommets);
	    }
	}
    }
    
    /**
     * Retourne le graphe partiel (celui affiché dans le panneau graphique) de ce graphe
     * 
     * @return
     */
    public Graphe getPartiel() {
	Graphe graphePartiel = new Graphe("partiel de " + this.nom);
	graphePartiel.setOriente(this.isOriente());
	graphePartiel.setPonderationArc(this.isPonderationArc());
	for (Sommet sommet : this.listeSommets) {
	    if (sommet.isVisible()) {
		graphePartiel.ajouterSommet(sommet);
	    }
	}
	for (Arc arc : this.listeArcs) {
	    if (arc.getOrigine().isVisible() && arc.getDestination().isVisible()) {
		graphePartiel.ajouterArc(arc);
	    }
	}
	return graphePartiel;
    }
    
    /**
     * Retourne TRUE si ce graphe est complet sinon FALSE.
     * 
     * @return
     */
    public boolean isComplet() {
	int deg = this.getOrdre() - 1;
	for (Sommet sommet : listeSommets) {
	    if (this.getSuccesseurs(sommet).size() < deg) {
		return false;
	    }
	}
	return true;
    }

    public HashMap<Sommet, ArrayList<Sommet>> getAllSuccesseurs() {
	HashMap<Sommet, ArrayList<Sommet>> successeursMap = new HashMap<Sommet, ArrayList<Sommet>>();
	for (Sommet sommet : listeSommets) {
	    successeursMap.put(sommet, this.getSuccesseurs(sommet));
	}
	return successeursMap;
    }

    public void ajouterArc(Arc arc) {
	this.listeArcs.add(arc);
    }

    public void supprimerArc(Arc arcASupp) {
	this.listeArcs.remove(arcASupp);
    }

    public void renomerArc(Arc arc, String nouvNom) {
	int index = this.listeArcs.indexOf(arc);
	if (index >= 0) {
	    this.listeArcs.get(index).setLabel(nouvNom);
	}
    }

    public void definirValeurPonderation(Arc arc, float nouvValeur) {
	int index = this.listeArcs.indexOf(arc);
	if (index >= 0) {
	    this.listeArcs.get(index).setValeurPonderation(nouvValeur);
	}
    }

    public float getValeurPonderation(Arc arc) {
	int index = this.listeArcs.indexOf(arc);
	if (index >= 0) {
	    return this.listeArcs.get(index).getValeurPonderation();
	} else {
	    return 0;
	}
    }

    public String getNom() {
	return nom;
    }

    public void setNom(String nom) {
	this.nom = nom;
    }

    public int getOrdre() {
	return this.listeSommets.size();
    }

    public int getTaille() {
	return this.listeArcs.size();
    }

    public boolean isOriente() {
	return oriente;
    }

    public void setOriente(boolean oriente) {
	this.oriente = oriente;
    }

    public ArrayList<Sommet> getListeSommets() {
	return listeSommets;
    }

    public ArrayList<Arc> getListeArcs() {
	return listeArcs;
    }

    public boolean isPonderationArc() {
	return ponderationArc;
    }

    public void setPonderationArc(boolean ponderationArc) {
	this.ponderationArc = ponderationArc;
    }

    @Override
    public String toString() {
	return this.nom;
    }
    
    /**
     * Cette fonction renvoie TRUE si ce graphe est biparti, sinon FALSE.
     * 
     * @return
     */
    public boolean isBiParti() {
	this.mapColoration = new HashMap<Sommet, Integer>();
	for (Sommet sommet : this.listeSommets) {
	    int couleur = recupererPlusPetiteCouleurDispo(recupererCouleursVoisines(sommet));
	    mapColoration.put(sommet, couleur);
	    if (couleur > 2) {
		return false;
	    }
	}
	return true;
    }

    private ArrayList<Integer> recupererCouleursVoisines(Sommet sommet) {
	ArrayList<Integer> couleursVoisines = new ArrayList<Integer>();
	ArrayList<Sommet> sommetsVoisins = this.getSommetsAdjacents(sommet);
	for (Sommet sommetVoisin : sommetsVoisins) {
	    Integer couleur = this.mapColoration.get(sommetVoisin);
	    if (couleur != null && !couleursVoisines.contains(couleur)) {
		couleursVoisines.add(couleur);
	    }
	}
	return couleursVoisines;
    }

    private int recupererPlusPetiteCouleurDispo(ArrayList<Integer> couleursVoisines) {
	int couleur = 1;
	while (couleursVoisines.contains(couleur)) {
	    couleur++;
	}
	return couleur;
    }
}
