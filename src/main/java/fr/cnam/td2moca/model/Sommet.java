package fr.cnam.td2moca.model;

import java.awt.geom.Ellipse2D;

/**
 * @author Christopher MILAZZO
 */
public class Sommet extends Ellipse2D.Double {

    private static final long serialVersionUID = 1L;
    public static final int RADIUS = 20;

    private String label;
    private boolean visible;

    public Sommet(String nom) {
	super(100, 100, RADIUS, RADIUS);
	this.label = nom;
	this.visible = false;
    }

    public String getLabel() {
	return label;
    }

    public void setLabel(String label) {
	this.label = label;
    }

    @Override
    public String toString() {
	return this.label + (this.visible ? " (v)" : " (c)");
    }

    @Override
    public boolean equals(Object obj) {
	try {
	    Sommet autre = (Sommet) obj;
	    return (autre.getLabel().equals(this.label)) ? true : false;
	} catch (Exception e) {
	    return false;
	}
    }

    public boolean isVisible() {
	return visible;
    }

    public void setVisible(boolean visible) {
	this.visible = visible;
    }
}
