package fr.cnam.td2moca.model;

import java.awt.geom.Line2D;

/**
 * @author Christopher MILAZZO
 */
public class Arc extends Line2D.Double {

    private static final long serialVersionUID = 1L;
    private String label;
    private float valeurPonderation;
    private Sommet origine;
    private Sommet destination;

    public Arc(Sommet sommetOrigine, Sommet sommetDest) {
	this(null, sommetOrigine, sommetDest);
    }

    public Arc(String nom, Sommet sommetOrigine, Sommet sommetDest) {
	super(sommetOrigine.x + (Sommet.RADIUS / 2), sommetOrigine.y + (Sommet.RADIUS / 2),
		sommetDest.x + (Sommet.RADIUS / 2), sommetDest.y + (Sommet.RADIUS / 2));
	this.label = nom;
	this.origine = sommetOrigine;
	this.destination = sommetDest;
	this.valeurPonderation = 0;
    }

    public String getLabel() {
	return label;
    }

    public void setLabel(String label) {
	this.label = label;
    }

    public float getValeurPonderation() {
	return valeurPonderation;
    }

    public void setValeurPonderation(float valeurPonderation) {
	this.valeurPonderation = valeurPonderation;
    }

    public Sommet getOrigine() {
	return origine;
    }

    public void setOrigine(Sommet origine) {
	this.origine = origine;
    }

    public Sommet getDestination() {
	return destination;
    }

    public void setDestination(Sommet destination) {
	this.destination = destination;
    }

    /**
     * Met à jour les coordonnées de l'arc en fonction des sommets d'origine et de
     * destination
     */
    public void syncSommets() {
	this.x1 = this.origine.x + (Sommet.RADIUS / 2);
	this.y1 = this.origine.y + (Sommet.RADIUS / 2);
	this.x2 = this.destination.x + (Sommet.RADIUS / 2);
	this.y2 = this.destination.y + (Sommet.RADIUS / 2);
    }

    @Override
    public String toString() {
	return (this.label != null) ? "(" + this.label + ")" : "(anonyme)";
    }

    @Override
    public boolean equals(Object obj) {
	try {
	    Arc autreArc = (Arc) obj;
	    return (autreArc.origine.equals(this.origine) && autreArc.destination.equals(this.destination)) ? true
		    : false;
	} catch (Exception e) {
	    return false;
	}
    }
}
