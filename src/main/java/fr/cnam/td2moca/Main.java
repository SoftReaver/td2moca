package fr.cnam.td2moca;

import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JTable;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.table.DefaultTableModel;

import fr.cnam.td2moca.actions.ActionAbout;
import fr.cnam.td2moca.actions.ActionAfficherTousLesSommets;
import fr.cnam.td2moca.actions.ActionAjouterArc;
import fr.cnam.td2moca.actions.ActionAjouterArcSurSommet;
import fr.cnam.td2moca.actions.ActionAjouterSommet;
import fr.cnam.td2moca.actions.ActionAnnulerCommande;
import fr.cnam.td2moca.actions.ActionCacherTousLesSommets;
import fr.cnam.td2moca.actions.ActionChargerGraphe;
import fr.cnam.td2moca.actions.ActionColoriageAlgo;
import fr.cnam.td2moca.actions.ActionDefinirArcsPonderes;
import fr.cnam.td2moca.actions.ActionEnregistrerGraphe;
import fr.cnam.td2moca.actions.ActionEnregistrerGrapheSous;
import fr.cnam.td2moca.actions.ActionFermer;
import fr.cnam.td2moca.actions.ActionHelp;
import fr.cnam.td2moca.actions.ActionKruskalAlgo;
import fr.cnam.td2moca.actions.ActionListeVoisinsAlgo;
import fr.cnam.td2moca.actions.ActionModifierGrapheOrientation;
import fr.cnam.td2moca.actions.ActionNouveau;
import fr.cnam.td2moca.actions.ActionParcoursBFSAlgo;
import fr.cnam.td2moca.actions.ActionParcoursDFSAlgo;
import fr.cnam.td2moca.actions.ActionQuitter;
import fr.cnam.td2moca.actions.ActionRefaireCommande;
import fr.cnam.td2moca.actions.ActionRenommerGraphe;
import fr.cnam.td2moca.actions.ActionSupprimerArc;
import fr.cnam.td2moca.actions.ActionSupprimerSommet;
import fr.cnam.td2moca.gui.LP;
import fr.cnam.td2moca.gui.LS;
import fr.cnam.td2moca.gui.MenuBar;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.gui.PopupGraphicPanel;
import fr.cnam.td2moca.gui.PopupListeSommets;
import fr.cnam.td2moca.handlers.HandlerQuitter;
import fr.cnam.td2moca.handlers.LSTableModelListenerHandler;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Main {

    private static final int MAIN_WINDOW_WIDTH = 800;
    private static final int MAIN_WINDOW_HEIGHT = 600;

    public static final String MAIN_WINDOW_TITLE = "Graphe";
    public static final String LISTE_SOMMETS_JLIST = "liste_sommets_id";
    public static final String LISTE_SUCCESSEURS_JTABLE = "liste_successeurs_id";
    public static final String POPUP_GRAPHIC_PANEL_JPOPUPMENU = "popup_graphic_panel_id";
    public static final String MENU_ITEM_AJOUTER_ARC_JMENUITEM = "menu_item_ajouter_arc_id";
    public static final String POPUP_LISTE_SOMMETS_JPOPUPMENU = "popup_liste_sommets_id";
    public static final String PANNEAU_GRAPHIQUE_ID = "panneau_graphique_id";
    public static final String MENU_SUPP_SOMMETS_JMENU = "menu_supp_sommets_id";
    public static final String MENU_SUPP_ARCS_JMENU = "menu_supp_arcs_id";
    public static final String MENU_ITEM_ANNULER_JMENUITEM = "menu_item_annuler_id";
    public static final String MENU_ITEM_REFAIRE_JMENUITEM = "menu_item_refaire_id";
    public static final String MENU_CHECKBOX_ORIENTE_JCHECKBOXMENUITEM = "menu_checkbox_oriente_id";
    public static final String MENU_CHECKBOX_PONDERE_JCHECKBOXMENUITEM = "menu_checkbox_pondere_id";

    private Map<String, Component> componentsRefMap;

    private Graphe graphe;
    private boolean needToSave;
    private JFrame frame;
    private JTable table;
    private Invocateur invocateur;
    private String nomFichierCourant;
    private ArrayList<Integer> LS;
    private int positionXNouvSommet;
    private int positionYNouvSommet;
    private boolean modeAjoutArcSurSommet;

    // Actions
    public final ActionNouveau actionNouveau;
    public final ActionChargerGraphe actionChargerGraphe;
    public final ActionEnregistrerGraphe actionEnregistrerGraphe;
    public final ActionEnregistrerGrapheSous actionEnregistrerGrapheSous;
    public final ActionAjouterArc actionAjouterArc;
    public ActionAjouterArcSurSommet actionAjouterArcSurSommet;
    public final ActionAjouterSommet actionAjouterSommet;
    public final ActionQuitter actionQuitter;
    public final ActionFermer actionFermer;
    public final ActionAfficherTousLesSommets actionAfficherTousLesSommets;
    public final ActionCacherTousLesSommets actionCacherTousLesSommets;
    public final ActionAnnulerCommande actionAnnulerCommande;
    public final ActionRefaireCommande actionRefaireCommande;
    public final ActionModifierGrapheOrientation actionModifierGrapheOrientation;
    public final ActionDefinirArcsPonderes actionDefinirArcsPonderes;
    public final ActionRenommerGraphe actionRenommerGraphe;
    public final ActionAbout actionAbout;
    public final ActionHelp actionHelp;
    public final ActionListeVoisinsAlgo actionListeVoisinsAlgo;
    public final ActionColoriageAlgo actionColoriageAlgo;
    public final ActionParcoursBFSAlgo actionParcoursBFSAlgo;
    public final ActionParcoursDFSAlgo actionParcoursDFSAlgo;
    public final ActionKruskalAlgo actionKruskalAlgo;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    Main window = new Main();
		    window.frame.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the application.
     * 
     * @throws IOException
     */
    public Main() throws IOException {
	this.componentsRefMap = new HashMap<String, Component>();
	this.invocateur = new Invocateur(this);
	this.LS = new ArrayList<Integer>();
	this.positionXNouvSommet = 100;
	this.positionYNouvSommet = 100;
	this.graphe = new Graphe("Untitled");
	this.setNeedToSave(false);
	this.modeAjoutArcSurSommet = false;

	this.actionNouveau = new ActionNouveau(this.invocateur, this);
	this.actionChargerGraphe = new ActionChargerGraphe(this.invocateur, this);
	this.actionEnregistrerGraphe = new ActionEnregistrerGraphe(this.invocateur, this);
	this.actionEnregistrerGrapheSous = new ActionEnregistrerGrapheSous(this.invocateur, this);
	this.actionAjouterArcSurSommet = null;
	this.actionAjouterArc = new ActionAjouterArc(this.invocateur, this);
	this.actionAjouterSommet = new ActionAjouterSommet(this.invocateur, this);
	this.actionFermer = new ActionFermer(this.invocateur, this);
	this.actionQuitter = new ActionQuitter(this.invocateur, this);
	this.actionAfficherTousLesSommets = new ActionAfficherTousLesSommets(this.invocateur, this);
	this.actionCacherTousLesSommets = new ActionCacherTousLesSommets(this.invocateur, this);
	this.actionAnnulerCommande = new ActionAnnulerCommande(this.invocateur, this);
	this.actionRefaireCommande = new ActionRefaireCommande(this.invocateur, this);
	this.actionModifierGrapheOrientation = new ActionModifierGrapheOrientation(this.invocateur, this);
	this.actionDefinirArcsPonderes = new ActionDefinirArcsPonderes(this.invocateur, this);
	this.actionRenommerGraphe = new ActionRenommerGraphe(this.invocateur, this);
	this.actionAbout = new ActionAbout(this.invocateur, this);
	this.actionHelp = new ActionHelp(this.invocateur, this);
	this.actionListeVoisinsAlgo = new ActionListeVoisinsAlgo(this.invocateur, this);
	this.actionColoriageAlgo = new ActionColoriageAlgo(this.invocateur, this);
	this.actionParcoursBFSAlgo = new ActionParcoursBFSAlgo(this.invocateur, this);
	this.actionParcoursDFSAlgo = new ActionParcoursDFSAlgo(this.invocateur, this);
	this.actionKruskalAlgo = new ActionKruskalAlgo(this.invocateur, this);

	initialize();
	sync();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
	initFrame();
	initLayout();
	initMenuBar();
	initPanneauGraphique();
	initLP();
	initLS();
	initPopupGraphicPanel();
	initPopupListeSommets();
    }

    public boolean isNeedToSave() {
	return needToSave;
    }

    public Invocateur getInvocateur() {
	return this.invocateur;
    }

    public void setNeedToSave(boolean needToSave) {
	this.needToSave = needToSave;
    }

    public int getPositionXNouvSommet() {
	return positionXNouvSommet;
    }

    public int getPositionYNouvSommet() {
	return positionYNouvSommet;
    }

    public Graphe getGraphe() {
	return graphe;
    }

    public String getNomFichierCourant() {
	return nomFichierCourant;
    }

    public void setNomFichierCourant(String nomFichierCourant) {
	this.nomFichierCourant = nomFichierCourant;
    }

    public JFrame getFrame() {
	return this.frame;
    }

    public PanneauGraphique getPanneauGraphique() {
	return (PanneauGraphique) this.componentsRefMap.get(PANNEAU_GRAPHIQUE_ID);
    }

    public ArrayList<Integer> getLS() {
	return this.LS;
    }

    public Map<String, Component> getComponentsRefMap() {
	return this.componentsRefMap;
    }

    // ============== ACTIONS ==================

    /**
     * Synchronise toutes les données avec l'interface
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void sync() {
	// MAJ du titre de la fenêtre principale
	int beg = (this.nomFichierCourant != null) ? this.nomFichierCourant.lastIndexOf("/") : 0;
	if (beg == -1) {
	    beg = 0;
	}

	int end = (this.nomFichierCourant != null) ? this.nomFichierCourant.lastIndexOf(".xml") : 0;
	if (end == -1) {
	    end = nomFichierCourant.length();
	}
	this.frame.setTitle(((this.needToSave) ? "* " : "") + MAIN_WINDOW_TITLE + " - " + this.graphe.getNom()
		+ ((this.nomFichierCourant == null) ? "" : " - " + this.nomFichierCourant.substring(beg, end)));

	// MAJ de la liste des sommets
	((DefaultListModel) ((JList<Sommet>) this.componentsRefMap.get(LISTE_SOMMETS_JLIST)).getModel())
		.removeAllElements();
	DefaultListModel listModel = ((DefaultListModel) ((JList<Sommet>) this.componentsRefMap.get(LISTE_SOMMETS_JLIST)).getModel());
	
	for (Sommet sommet : this.graphe.getListeSommets()) {
	    listModel.addElement(sommet);
	}

	// MAJ de la liste des successeurs
	HashMap<Sommet, ArrayList<Sommet>> successeursMap = this.graphe.getAllSuccesseurs();
	Iterator<Entry<Sommet, ArrayList<Sommet>>> iterator = successeursMap.entrySet().iterator();
	ArrayList<Sommet> sommets = new ArrayList<Sommet>();
	while (iterator.hasNext()) {
	    sommets.add(iterator.next().getKey());
	}
	Collections.sort(sommets, new Comparator<Sommet>() {
	    @Override
	    public int compare(Sommet lhs, Sommet rhs) {
		return lhs.getLabel().compareTo(rhs.getLabel());
	    }

	});

	int tailleLS = (this.graphe.isOriente()) ? this.graphe.getTaille() : this.graphe.getTaille() * 2;
	Object[][] data = new Object[1][tailleLS];
	String[] headers = new String[tailleLS];
	this.LS.clear();

	int colIndex = 0;
	for (Sommet sommetCourant : sommets) {
	    for (Sommet sommetSuccesseur : successeursMap.get(sommetCourant)) {
		colIndex++;
		headers[colIndex - 1] = sommetCourant.getLabel();
		data[0][colIndex - 1] = Integer.valueOf(sommetSuccesseur.getLabel());
		this.LS.add(Integer.valueOf(sommetSuccesseur.getLabel()));
	    }
	}
	table.setModel(new DefaultTableModel(data, headers));
	table.getModel().addTableModelListener(new LSTableModelListenerHandler(this));

	// MAJ de la représentation graphique du graphe
	((PanneauGraphique) this.componentsRefMap.get(PANNEAU_GRAPHIQUE_ID)).sync();

	// MAJ du checkbox graphe orienté
	((JCheckBoxMenuItem) this.componentsRefMap.get(MENU_CHECKBOX_ORIENTE_JCHECKBOXMENUITEM))
		.setSelected(this.graphe.isOriente());

	// MAJ du checkbox graphe pondéré
	((JCheckBoxMenuItem) this.componentsRefMap.get(MENU_CHECKBOX_PONDERE_JCHECKBOXMENUITEM))
		.setSelected(this.graphe.isPonderationArc());

	// MAJ du menu supprimmer sommets
	((JMenu) this.componentsRefMap.get(MENU_SUPP_SOMMETS_JMENU)).removeAll();
	for (Sommet sommet : this.graphe.getListeSommets()) {
	    JMenuItem mnItem = new JMenuItem(sommet.getLabel());
	    mnItem.addActionListener(new ActionSupprimerSommet(sommet, this.invocateur, this));
	    ((JMenu) this.componentsRefMap.get(MENU_SUPP_SOMMETS_JMENU)).add(mnItem);
	}

	// MAJ du menu supprimer arcs
	((JMenu) this.componentsRefMap.get(MENU_SUPP_ARCS_JMENU)).removeAll();
	for (Arc arc : this.graphe.getListeArcs()) {
	    JMenuItem mnItem = new JMenuItem(
		    arc.toString() + " " + arc.getOrigine().getLabel() + " -> " + arc.getDestination().getLabel());
	    mnItem.addActionListener(new ActionSupprimerArc(arc, this.invocateur, this));
	    ((JMenu) this.componentsRefMap.get(MENU_SUPP_ARCS_JMENU)).add(mnItem);
	}

	// MAJ des bouton Refaire
	JMenuItem mntRefaire = ((JMenuItem) this.componentsRefMap.get(MENU_ITEM_REFAIRE_JMENUITEM));
	if (this.invocateur.hasProchaineCommande()) {
	    mntRefaire.setText("Refaire " + this.invocateur.nomProchaineCommande());
	    mntRefaire.setEnabled(true);
	} else {
	    mntRefaire.setText("Refaire");
	    mntRefaire.setEnabled(false);
	}

	// MAJ du bouton Annuler
	JMenuItem mntAnnuler = ((JMenuItem) this.componentsRefMap.get(MENU_ITEM_ANNULER_JMENUITEM));
	if (this.invocateur.hasCommandePrecedente()) {
	    mntAnnuler.setText("Annuler " + this.invocateur.nomCommandePrecedente());
	    mntAnnuler.setEnabled(true);
	} else {
	    mntAnnuler.setText("Annuler");
	    mntAnnuler.setEnabled(false);
	}
    }

    /**
     * Définit le graphe courant
     * 
     * @param graphe
     */
    public void definirGraphe(Graphe graphe) {
	this.graphe = graphe;
    }

    /**
     * Affiche le menu contextuel pour le panneau graphique
     * 
     * @param me
     */
    public void showPopup(MouseEvent me, Sommet sommetSelectionne) {
	this.positionXNouvSommet = me.getX();
	this.positionYNouvSommet = me.getY();
	JMenuItem menuItem = (JMenuItem) componentsRefMap.get(MENU_ITEM_AJOUTER_ARC_JMENUITEM);

	if (sommetSelectionne == null) {
	    if (this.modeAjoutArcSurSommet) {
		this.modeAjoutArcSurSommet = false;
		menuItem.setText("Ajouter arc");
		if (this.actionAjouterArcSurSommet != null) {
		    menuItem.removeActionListener(this.actionAjouterArcSurSommet);
		}
		menuItem.addActionListener(this.actionAjouterArc);
	    }
	} else {
	    this.modeAjoutArcSurSommet = true;
	    menuItem.setText("Ajouter arc au sommet " + sommetSelectionne.getLabel());
	    if (this.actionAjouterArcSurSommet != null) {
		menuItem.removeActionListener(this.actionAjouterArcSurSommet);
	    }
	    this.actionAjouterArcSurSommet = new ActionAjouterArcSurSommet(this.invocateur, this, sommetSelectionne);
	    menuItem.removeActionListener(this.actionAjouterArc);
	    menuItem.addActionListener(this.actionAjouterArcSurSommet);
	}

	((JPopupMenu) this.componentsRefMap.get(POPUP_GRAPHIC_PANEL_JPOPUPMENU)).show(me.getComponent(), me.getX(),
		me.getY());
    }

    public void showSommetsPopup(MouseEvent me) {
	((JPopupMenu) this.componentsRefMap.get(POPUP_LISTE_SOMMETS_JPOPUPMENU)).show(me.getComponent(), me.getX(),
		me.getY());
    }

    public void effacerHistorique() {
	this.invocateur.effacerHistorique();
    }

    /**
     * Arrêt du programme
     */
    public void exit() {
	this.frame.dispose();
    }

    // ============= GUI INITIALISATION ===============

    private void initFrame() {
	frame = new JFrame(MAIN_WINDOW_TITLE);
	frame.setBounds(100, 100, MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
	frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	frame.addWindowListener(new HandlerQuitter(this.invocateur, this));
    }

    private void initLayout() {
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[] { 100, 327, 0 };
	gridBagLayout.rowHeights = new int[] { 60, 1000, 0 };
	gridBagLayout.columnWeights = new double[] { 1.0, 5.0, Double.MIN_VALUE };
	gridBagLayout.rowWeights = new double[] { 0.0, 10.0, Double.MIN_VALUE };
	frame.getContentPane().setLayout(gridBagLayout);
    }

    private void initMenuBar() {
	new MenuBar(this);
    }

    private void initPanneauGraphique() {
	JPanel panneauGraphique = new PanneauGraphique(this);
	panneauGraphique.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	GridBagConstraints gbc_panel = new GridBagConstraints();
	gbc_panel.fill = GridBagConstraints.BOTH;
	gbc_panel.gridx = 1;
	gbc_panel.gridy = 1;
	frame.getContentPane().add(panneauGraphique, gbc_panel);
	this.componentsRefMap.put(PANNEAU_GRAPHIQUE_ID, panneauGraphique);
    }

    private void initLP() {
	this.componentsRefMap.put(LISTE_SOMMETS_JLIST, new LP(this));
    }

    private void initLS() {
	this.table = new LS(this);
	this.componentsRefMap.put(LISTE_SUCCESSEURS_JTABLE, table);
    }

    private void initPopupGraphicPanel() {
	this.componentsRefMap.put(POPUP_GRAPHIC_PANEL_JPOPUPMENU, new PopupGraphicPanel(this));
    }

    private void initPopupListeSommets() {
	this.componentsRefMap.put(POPUP_LISTE_SOMMETS_JPOPUPMENU, new PopupListeSommets(this));
    }
}
