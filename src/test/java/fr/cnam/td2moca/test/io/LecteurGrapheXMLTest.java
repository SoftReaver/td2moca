package fr.cnam.td2moca.test.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Objects;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.io.LecteurGrapheXML;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class LecteurGrapheXMLTest {
    
    private static LecteurGrapheXML lecteurGraphe;
    private static Graphe expectedGraphe1;
    private static Graphe expectedGraphe2;
    
    @BeforeClass
    public static void init() {
	lecteurGraphe = new LecteurGrapheXML();
	
	Sommet s1 = new Sommet("1");
	Sommet s2 = new Sommet("2");
	Sommet s3 = new Sommet("3");
	Sommet s4 = new Sommet("4");
	Sommet s5 = new Sommet("5");
	Sommet s6 = new Sommet("6");
	Sommet s7 = new Sommet("7");
	
	expectedGraphe1 = new Graphe("graphe1");
	expectedGraphe1.setOriente(true);
	expectedGraphe1.setPonderationArc(true);
	
	expectedGraphe1.ajouterSommet(s1);
	expectedGraphe1.ajouterSommet(s2);
	expectedGraphe1.ajouterSommet(s3);
	expectedGraphe1.ajouterSommet(s4);
	expectedGraphe1.ajouterSommet(s5);
	
	expectedGraphe1.ajouterArc(new Arc(s1,s2));
	expectedGraphe1.ajouterArc(new Arc(s2,s4));
	expectedGraphe1.ajouterArc(new Arc(s4,s3));
	expectedGraphe1.ajouterArc(new Arc(s3,s1));
	Arc a1to5 = new Arc(s1,s5);
	a1to5.setValeurPonderation((float) 6.9);
	expectedGraphe1.ajouterArc(a1to5);
	expectedGraphe1.ajouterArc(new Arc(s2,s5));
	expectedGraphe1.ajouterArc(new Arc(s3,s5));
	Arc a4to5 = new Arc(s4,s5);
	a4to5.setValeurPonderation(12);
	expectedGraphe1.ajouterArc(a4to5);
		
	expectedGraphe2 = new Graphe("graphe2");
	expectedGraphe2.setOriente(false);
	expectedGraphe2.setPonderationArc(false);
	
	expectedGraphe2.ajouterSommet(s1);
	expectedGraphe2.ajouterSommet(s2);
	expectedGraphe2.ajouterSommet(s3);
	expectedGraphe2.ajouterSommet(s6);
	expectedGraphe2.ajouterSommet(s7);
	
	expectedGraphe2.ajouterArc(new Arc(s1,s3));
	expectedGraphe2.ajouterArc(new Arc(s1,s6));
	expectedGraphe2.ajouterArc(new Arc(s1,s7));
	expectedGraphe2.ajouterArc(new Arc(s2,s1));
	expectedGraphe2.ajouterArc(new Arc(s2,s3));
	expectedGraphe2.ajouterArc(new Arc(s2,s6));
	expectedGraphe2.ajouterArc(new Arc(s2,s7));
	expectedGraphe2.ajouterArc(new Arc(s6,s3));
	expectedGraphe2.ajouterArc(new Arc(s7,s3));
	expectedGraphe2.ajouterArc(new Arc(s7,s6));
    }
    
    @Test
    public void graphe1() {
	Graphe graphe1 = lecteurGraphe.chargerGraphe("src/test/resources/graphe1.xml");
	assertNotNull(graphe1);
	compareGraphe(expectedGraphe1, graphe1);
    }
    
    @Test
    public void graphe2() {
	Graphe graphe2= lecteurGraphe.chargerGraphe("src/test/resources/graphe2.xml");
	assertNotNull(graphe2);
	compareGraphe(expectedGraphe2, graphe2);
    }
    
    @Test
    public void grapheErrone() {
	assertThrows(RuntimeException.class, () -> {
	    lecteurGraphe.chargerGraphe("src/test/resources/errone.xml");
	});
    }

    /**
     * Compare les deux graphes donnés en utilsant les assertions
     * 
     * @param g1
     * @param g2
     */
    private void compareGraphe(Graphe g1, Graphe g2) {
	Objects.requireNonNull(g1);
	Objects.requireNonNull(g2);
	
	assertEquals(g1.isOriente(), g2.isOriente());
	assertEquals(g1.isPonderationArc(), g2.isPonderationArc());
	assertEquals(g1.getNom(), g2.getNom());
	
	// Les sommets
	for (Sommet sommet : g1.getListeSommets()) {
	    assertTrue(g2.getListeSommets().contains(sommet));
	}
	assertEquals(g1.getListeSommets().size(), g2.getListeSommets().size());
	
	// Les arcs
	for (Arc arc : g1.getListeArcs()) {
	    assertTrue(g2.getListeArcs().contains(arc));
	    assertEquals(g1.getValeurPonderation(arc), g2.getValeurPonderation(arc), 0.001);
	}
	assertEquals(g1.getListeArcs().size(), g2.getListeArcs().size());
    }
}
