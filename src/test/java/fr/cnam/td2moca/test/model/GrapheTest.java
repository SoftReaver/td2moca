package fr.cnam.td2moca.test.model;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class GrapheTest {

    private Graphe graphe;
    private static Graphe grapheCompletNOriente;
    private static Graphe grapheNonCompletNOriente;
    private static Graphe grapheCompletOriente;
    private static Graphe grapheNonCompletOriente;
    private static Graphe grapheBiparti;
    private static Graphe grapheNonBiparti;
    private static Graphe G;
    private static Graphe partielDeG;
    private static Sommet s1;
    private static Sommet s2;
    private static Sommet s3;
    private static Sommet s4;

    private static Arc a1;
    private static Arc a2;
    private static Arc a3;
    private static Arc a4;
    private static Arc a5;
    private static Arc a6;
    private static Arc a7;
    private static Arc a8;

    @BeforeClass
    public static void initDonnees() {
	// Les sommets
	s1 = new Sommet("s1");
	s1.setVisible(true);
	s2 = new Sommet("s2");
	s2.setVisible(false);
	s3 = new Sommet("s3");
	s3.setVisible(true);
	s4 = new Sommet("s4");
	s4.setVisible(true);

	// Les arcs
	a1 = new Arc(s2, s1);
	a2 = new Arc(s3, s1);
	a3 = new Arc(s3, s2);
	a4 = new Arc(s1, s2);
	a5 = new Arc(s1, s3);
	a6 = new Arc(s2, s3);
	a7 = new Arc(s2, s4);
	a8 = new Arc(s3, s4);

	grapheCompletNOriente = new Graphe("graphe complet non orienté");
	grapheCompletNOriente.setOriente(false);
	grapheCompletNOriente.ajouterSommet(s1);
	grapheCompletNOriente.ajouterSommet(s2);
	grapheCompletNOriente.ajouterSommet(s3);
	grapheCompletNOriente.ajouterArc(a4);
	grapheCompletNOriente.ajouterArc(a5);
	grapheCompletNOriente.ajouterArc(a6);

	grapheCompletOriente = new Graphe("graphe complet orienté");
	grapheCompletOriente.setOriente(true);
	grapheCompletOriente.ajouterSommet(s1);
	grapheCompletOriente.ajouterSommet(s2);
	grapheCompletOriente.ajouterSommet(s3);
	grapheCompletOriente.ajouterArc(a1);
	grapheCompletOriente.ajouterArc(a2);
	grapheCompletOriente.ajouterArc(a3);
	grapheCompletOriente.ajouterArc(a4);
	grapheCompletOriente.ajouterArc(a5);
	grapheCompletOriente.ajouterArc(a6);

	grapheNonCompletNOriente = new Graphe("graphe non complet et non orienté");
	grapheNonCompletNOriente.setOriente(false);
	grapheNonCompletNOriente.ajouterSommet(s1);
	grapheNonCompletNOriente.ajouterSommet(s2);
	grapheNonCompletNOriente.ajouterSommet(s3);
	grapheNonCompletNOriente.ajouterSommet(s4);
	grapheNonCompletNOriente.ajouterArc(a4);
	grapheNonCompletNOriente.ajouterArc(a5);
	grapheNonCompletNOriente.ajouterArc(a6);

	grapheNonCompletOriente = new Graphe("graphe non complet orienté");
	grapheNonCompletOriente.setOriente(true);
	grapheNonCompletOriente.ajouterSommet(s1);
	grapheNonCompletOriente.ajouterSommet(s2);
	grapheNonCompletOriente.ajouterSommet(s3);
	grapheNonCompletOriente.ajouterSommet(s4);
	grapheNonCompletOriente.ajouterArc(a1);
	grapheNonCompletOriente.ajouterArc(a2);
	grapheNonCompletOriente.ajouterArc(a3);
	grapheNonCompletOriente.ajouterArc(a4);
	grapheNonCompletOriente.ajouterArc(a5);
	grapheNonCompletOriente.ajouterArc(a6);

	grapheBiparti = new Graphe("graphe biParti");
	grapheBiparti.ajouterSommet(s1);
	grapheBiparti.ajouterSommet(s2);
	grapheBiparti.ajouterSommet(s3);
	grapheBiparti.ajouterSommet(s4);
	grapheBiparti.ajouterArc(new Arc(s1, s2));
	grapheBiparti.ajouterArc(new Arc(s3, s4));

	G = new Graphe("G");
	G.setOriente(false);
	G.ajouterSommet(s1);
	G.ajouterSommet(s2);
	G.ajouterSommet(s3);
	G.ajouterSommet(s4);
	G.ajouterArc(a2);
	G.ajouterArc(a3);
	G.ajouterArc(a7);

	partielDeG = new Graphe("partiel de G");
	partielDeG.ajouterSommet(s1);
	partielDeG.ajouterSommet(s3);
	partielDeG.ajouterSommet(s4);
	partielDeG.ajouterArc(a2);

	grapheNonBiparti = new Graphe("graphe non biparti");
	grapheNonBiparti.ajouterSommet(s1);
	grapheNonBiparti.ajouterSommet(s2);
	grapheNonBiparti.ajouterSommet(s3);
	grapheNonBiparti.ajouterSommet(s4);
	grapheNonBiparti.ajouterArc(a2);
	grapheNonBiparti.ajouterArc(a3);
	grapheNonBiparti.ajouterArc(a7);
	grapheNonBiparti.ajouterArc(a8);
    }

    @Before
    public void initGraphe() {
	graphe = new Graphe("Graphe de test");
    }

    @Test
    public void testerAjouterSommet() {
	ArrayList<Sommet> sommets = new ArrayList<Sommet>();
	assertArrayEquals(sommets.toArray(), graphe.getListeSommets().toArray());

	Sommet sommet = new Sommet("1");
	sommets.add(sommet);
	graphe.ajouterSommet(sommet);
	assertArrayEquals(sommets.toArray(), graphe.getListeSommets().toArray());
    }

    @Test
    public void testerSupprimerSommet() {
	ArrayList<Sommet> sommets = new ArrayList<Sommet>();
	Sommet sommet = new Sommet("2");
	sommets.add(sommet);
	graphe.ajouterSommet(sommet);
	assertArrayEquals(sommets.toArray(), graphe.getListeSommets().toArray());

	sommets.remove(0);
	graphe.supprimerSommet(sommet);
	assertArrayEquals(sommets.toArray(), graphe.getListeSommets().toArray());
    }

    @Test
    public void testerRenomerSommet() {
	Sommet sommet = new Sommet("un_nom");
	graphe.ajouterSommet(sommet);

	assertEquals("un_nom", sommet.getLabel());

	graphe.renomerSommet(sommet, "nouveau_nom");
	assertEquals("nouveau_nom", sommet.getLabel());
    }

    @Test
    public void testerTrouverSommet() {
	Sommet sommet = new Sommet("1");
	graphe.ajouterSommet(sommet);
	assertEquals(null, graphe.trouverSommet("nop"));
	assertEquals(sommet, graphe.trouverSommet("1"));
    }

    @Test
    public void testerTrouverArc() {
	Sommet s1 = new Sommet("origine");
	Sommet s2 = new Sommet("destination");
	Sommet s3 = new Sommet("3");
	Arc arc = new Arc(s1, s2);
	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	graphe.ajouterArc(arc);
	assertEquals(null, graphe.trouverArc(s1, s3));
	assertEquals(arc, graphe.trouverArc(s1, s2));
    }

    @Test
    public void testerGetSuccesseurs() {
	Sommet s1 = new Sommet("1");
	Sommet s2 = new Sommet("2");
	Sommet s3 = new Sommet("3");
	Arc a1 = new Arc(s1, s2);
	Arc a2 = new Arc(s1, s3);
	Arc a3 = new Arc(s3, s2);

	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	graphe.ajouterArc(a1);
	graphe.ajouterArc(a2);
	graphe.ajouterArc(a3);

	ArrayList<Sommet> listeSommets = new ArrayList<Sommet>();

	// Cas d'un graphe non-orienté
	graphe.setOriente(false);
	listeSommets.add(s1);
	listeSommets.add(s2);
	assertArrayEquals(listeSommets.toArray(), graphe.getSuccesseurs(s3).toArray());
	listeSommets.clear();
	listeSommets.add(s2);
	listeSommets.add(s3);
	assertArrayEquals(listeSommets.toArray(), graphe.getSuccesseurs(s1).toArray());

	// Cas des graphes orientés
	graphe.setOriente(true);
	assertArrayEquals(listeSommets.toArray(), graphe.getSuccesseurs(s1).toArray());
	listeSommets.clear();
	listeSommets.add(s2);
	assertArrayEquals(listeSommets.toArray(), graphe.getSuccesseurs(s3).toArray());
    }

    @Test
    public void testerGetPredecesseurs() {
	Sommet s1 = new Sommet("1");
	Sommet s2 = new Sommet("2");
	Sommet s3 = new Sommet("3");
	Arc a1 = new Arc(s1, s2);
	Arc a2 = new Arc(s1, s3);
	Arc a3 = new Arc(s3, s2);

	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	graphe.ajouterArc(a1);
	graphe.ajouterArc(a2);
	graphe.ajouterArc(a3);

	ArrayList<Sommet> listeSommets = new ArrayList<Sommet>();

	// Cas d'un graphe non-orienté
	graphe.setOriente(false);
	listeSommets.add(s1);
	listeSommets.add(s2);
	assertArrayEquals(listeSommets.toArray(), graphe.getPredecesseurs(s3).toArray());
	listeSommets.clear();
	listeSommets.add(s2);
	listeSommets.add(s3);
	assertArrayEquals(listeSommets.toArray(), graphe.getPredecesseurs(s1).toArray());

	// Cas des graphes orientés
	graphe.setOriente(true);
	listeSommets.clear();
	assertArrayEquals(listeSommets.toArray(), graphe.getPredecesseurs(s1).toArray());
	listeSommets.add(s1);
	assertArrayEquals(listeSommets.toArray(), graphe.getPredecesseurs(s3).toArray());
    }

    @Test
    public void testerGetSommetsAdjacents() {
	ArrayList<Sommet> listeSommetsAdjacents = new ArrayList<Sommet>();
	listeSommetsAdjacents.add(s1);
	listeSommetsAdjacents.add(s2);
	assertArrayEquals(listeSommetsAdjacents.toArray(), G.getSommetsAdjacents(s3).toArray());
    }

    @Test
    public void testerAreSommetsConnectes() {
	assertFalse(partielDeG.areSommetsConnectes(s1, s2));
	assertTrue(partielDeG.areSommetsConnectes(s1, s3));
	assertTrue(G.areSommetsConnectes(s1, s4));
    }

    @Test
    public void testerGetPartiel() {
	Graphe partiel = G.getPartiel();

	assertArrayEquals(partielDeG.getListeSommets().toArray(), partiel.getListeSommets().toArray());
	assertArrayEquals(partielDeG.getListeArcs().toArray(), partiel.getListeArcs().toArray());
    }

    @Test
    public void testerIsComplet() {
	assertFalse(grapheNonCompletNOriente.isComplet());
	assertFalse(grapheNonCompletOriente.isComplet());

	assertTrue(grapheCompletNOriente.isComplet());
	assertTrue(grapheCompletOriente.isComplet());
    }

    @Test
    public void testerGetAllSuccesseurs() {
	Graphe graphe = new Graphe("graphe");
	graphe.setOriente(true);
	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	graphe.ajouterSommet(s3);
	graphe.ajouterSommet(s4);
	graphe.ajouterArc(a2);
	graphe.ajouterArc(a3);
	graphe.ajouterArc(a7);

	HashMap<Sommet, ArrayList<Sommet>> expectedMap = new HashMap<Sommet, ArrayList<Sommet>>();
	expectedMap.put(s1, new ArrayList<Sommet>());
	expectedMap.put(s2, new ArrayList<Sommet>());
	expectedMap.put(s3, new ArrayList<Sommet>());
	expectedMap.put(s4, new ArrayList<Sommet>());
	expectedMap.get(s2).add(s4);
	expectedMap.get(s3).add(s1);
	expectedMap.get(s3).add(s2);

	// cas d'un graphe orienté
	HashMap<Sommet, ArrayList<Sommet>> successeursMap = graphe.getAllSuccesseurs();

	assertArrayEquals(expectedMap.keySet().toArray(), successeursMap.keySet().toArray());

	for (Sommet sommet : expectedMap.keySet()) {
	    ArrayList<Sommet> expectedListeSommets = expectedMap.get(sommet);
	    ArrayList<Sommet> listeSommets = successeursMap.get(sommet);
	    assertNotNull(listeSommets);
	    for (Sommet successeur : expectedListeSommets) {		
		assertTrue(listeSommets.contains(successeur));
	    }
	    assertEquals(expectedListeSommets.size(), listeSommets.size());
	}

	// Cas d'un graphe non orienté
	graphe.setOriente(false);
	expectedMap.get(s1).add(s3);
	expectedMap.get(s2).add(s3);
	expectedMap.get(s4).add(s2);
	successeursMap = graphe.getAllSuccesseurs();
	
	assertArrayEquals(expectedMap.keySet().toArray(), successeursMap.keySet().toArray());
	
	for (Sommet sommet : expectedMap.keySet()) {
	    ArrayList<Sommet> expectedListeSommets = expectedMap.get(sommet);
	    ArrayList<Sommet> listeSommets = successeursMap.get(sommet);
	    assertNotNull(listeSommets);
	    for (Sommet successeur : expectedListeSommets) {		
		assertTrue(listeSommets.contains(successeur));
	    }
	    assertEquals(expectedListeSommets.size(), listeSommets.size());
	}
    }

    @Test
    public void testerAjouterArc() {
	Graphe graphe = new Graphe("graphe");
	graphe.ajouterArc(a1);

	assertEquals(a1, graphe.getListeArcs().get(0));
    }

    @Test
    public void testerSupprimerArc() {
	Graphe graphe = new Graphe("graphe");
	graphe.ajouterArc(a1);
	graphe.ajouterArc(a2);

	assertEquals(a1, graphe.getListeArcs().get(0));
	graphe.supprimerArc(a1);
	assertEquals(a2, graphe.getListeArcs().get(0));
	assertEquals(1, graphe.getListeArcs().size());
    }

    @Test
    public void testerRenomerArc() {
	Graphe graphe = new Graphe("graphe");
	graphe.ajouterArc(a1);
	graphe.ajouterArc(a2);
	a1.setLabel("avant");
	a2.setLabel("fixe");

	assertEquals("avant", graphe.getListeArcs().get(0).getLabel());
	assertEquals("fixe", graphe.getListeArcs().get(1).getLabel());
	graphe.renomerArc(a1, "apres");
	assertEquals("apres", graphe.getListeArcs().get(0).getLabel());
	assertEquals("fixe", graphe.getListeArcs().get(1).getLabel());
    }

    @Test
    public void testerDefinirValeurPonderation() {
	Graphe graphe = new Graphe("graphe");
	graphe.ajouterArc(a1);
	graphe.ajouterArc(a2);
	a2.setValeurPonderation(0);

	assertEquals(0, graphe.trouverArc(a2.getOrigine(), a2.getDestination()).getValeurPonderation(), 0);
	graphe.definirValeurPonderation(a2, (float) 6.9);
	assertEquals(6.9, graphe.trouverArc(a2.getOrigine(), a2.getDestination()).getValeurPonderation(), 0.001);
    }

    @Test
    public void testerGetValeurPonderation() {
	Graphe graphe = new Graphe("graphe");
	Arc arc = new Arc(s1, s2);
	arc.setValeurPonderation(3);
	graphe.ajouterArc(arc);

	assertEquals(3, graphe.getValeurPonderation(arc), 0.001);
    }

    @Test
    public void testerIsBiParti() {
	assertTrue(grapheBiparti.isBiParti());
	assertFalse(grapheNonBiparti.isBiParti());
    }
}
