package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.commandes.CommandeDefinirPonderation;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class CommandeDefinirPonderationTest {
    private static Graphe graphe;
    private static Sommet s1;
    private static Sommet s2;
    private static Arc a1;
    private static CommandeDefinirPonderation commande;
    
    @BeforeClass
    public static void init() {
	graphe = new Graphe("graphe");
	s1 = new Sommet("1");
	s2 = new Sommet("2");

	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	
	a1 = new Arc(s1, s2);
	a1.setValeurPonderation((float) 0.0);
	
	graphe.ajouterArc(a1);
	
	commande = new CommandeDefinirPonderation(graphe, a1, (float) 6.9);
    }
    
    @Test
    public void definirPonderation() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertEquals(0.0, a1.getValeurPonderation(), 0.001);
	commande.executer();
	assertEquals(6.9, a1.getValeurPonderation(), 0.001);
    }

    private void testerDefaire() {
	commande.defaire();
	assertEquals(0.0, a1.getValeurPonderation(), 0.001);
    }
}
