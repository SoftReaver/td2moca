package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.CommandeAjouterArc;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class CommandeAjouterArcTest {
    private static Graphe graphe;
    private static Sommet s1;
    private static Sommet s2;
    private static Sommet s3;
    private static Arc a1;
    private static Arc a2;
    private static PanneauGraphique panneauGraphique;
    private static CommandeAjouterArc commande;
    
    @BeforeClass
    public static void init() {
	try {
	    panneauGraphique = new PanneauGraphique(new Main());
	} catch (IOException e) {
	    e.printStackTrace();
	}
	graphe = new Graphe("graphe");
	s1 = new Sommet("1");
	s2 = new Sommet("2");
	s3 = new Sommet("3");

	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	graphe.ajouterSommet(s3);
	
	a1 = new Arc(s1, s2);
	a2 = new Arc(s2, s3);
	
	graphe.ajouterArc(a1);
	
	commande = new CommandeAjouterArc(a2, graphe, panneauGraphique);
    }
    
    @Test
    public void ajouterArc() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertEquals(1, graphe.getListeArcs().size());
	assertFalse(graphe.getListeArcs().contains(a2));
	commande.executer();
	assertEquals(2, graphe.getListeArcs().size());
	assertTrue(graphe.getListeArcs().contains(a2));
    }

    private void testerDefaire() {
	commande.defaire();
	assertEquals(1, graphe.getListeArcs().size());
	assertFalse(graphe.getListeArcs().contains(a2));
    }
}
