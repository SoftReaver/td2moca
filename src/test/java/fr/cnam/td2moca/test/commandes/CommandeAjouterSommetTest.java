package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.CommandeAjouterSommet;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class CommandeAjouterSommetTest {
    private static Graphe graphe;
    private static Sommet s1;
    private static Sommet s2;
    private static PanneauGraphique panneauGraphique;
    private static CommandeAjouterSommet commande;
    
    @BeforeClass
    public static void init() {
	try {
	    panneauGraphique = new PanneauGraphique(new Main());
	} catch (IOException e) {
	    e.printStackTrace();
	}
	graphe = new Graphe("graphe");
	s1 = new Sommet("1");
	s2 = new Sommet("2");

	graphe.ajouterSommet(s1);
	
	commande = new CommandeAjouterSommet(s2, graphe, panneauGraphique);
    }
    
    @Test
    public void ajouterSommet() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertEquals(1, graphe.getListeSommets().size());
	assertFalse(graphe.getListeSommets().contains(s2));
	commande.executer();
	assertEquals(2, graphe.getListeSommets().size());
	assertTrue(graphe.getListeSommets().contains(s2));
    }

    private void testerDefaire() {
	commande.defaire();
	assertEquals(1, graphe.getListeSommets().size());
	assertFalse(graphe.getListeSommets().contains(s2));
    }
}
