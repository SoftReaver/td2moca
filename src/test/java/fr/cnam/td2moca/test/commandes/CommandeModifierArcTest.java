package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.CommandeModifierArc;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Arc;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class CommandeModifierArcTest {
    private static PanneauGraphique panneauGraphique;
    private static Graphe graphe;
    private static Sommet s1;
    private static Sommet s2;
    private static Sommet s3;
    private static Arc a1;
    private static Arc a2;
    private static CommandeModifierArc commande;
    
    @BeforeClass
    public static void init() {
	try {
	    panneauGraphique = new PanneauGraphique(new Main());
	} catch (IOException e) {
	    e.printStackTrace();
	}
	graphe = new Graphe("graphe");
	s1 = new Sommet("1");
	s2 = new Sommet("2");
	s3 = new Sommet("3");

	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	
	a1 = new Arc(s1, s2);
	a2 = new Arc(s1, s3);
	
	graphe.ajouterArc(a1);
	
	commande = new CommandeModifierArc(a1, a2, graphe, panneauGraphique);
    }
    
    @Test
    public void modifierArc() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertEquals(a1, graphe.getListeArcs().get(0));
	commande.executer();
	assertEquals(a2, graphe.getListeArcs().get(0));
    }

    private void testerDefaire() {
	commande.defaire();
	assertEquals(a1, graphe.getListeArcs().get(0));
    }
}
