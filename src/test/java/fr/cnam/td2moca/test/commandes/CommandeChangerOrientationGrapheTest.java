package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.commandes.CommandeChangerOrientationGraphe;
import fr.cnam.td2moca.model.Graphe;

public class CommandeChangerOrientationGrapheTest {
    private static Graphe graphe;
    private static CommandeChangerOrientationGraphe commande;
    
    @BeforeClass
    public static void init() {
	graphe = new Graphe("graphe");
	graphe.setOriente(false);
	
	commande = new CommandeChangerOrientationGraphe(graphe, true);
    }
    
    @Test
    public void changerOrientationGraphe() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertFalse(graphe.isOriente());
	commande.executer();
	assertTrue(graphe.isOriente());
    }

    private void testerDefaire() {
	commande.defaire();
	assertFalse(graphe.isOriente());
    }
}
