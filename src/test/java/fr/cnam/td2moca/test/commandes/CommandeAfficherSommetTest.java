package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.CommandeAfficherSommet;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class CommandeAfficherSommetTest {
    private static Graphe graphe;
    private static Sommet sommet;
    private static PanneauGraphique panneauGraphique;
    private static CommandeAfficherSommet commande;
    
    @BeforeClass
    public static void init() {
	try {
	    panneauGraphique = new PanneauGraphique(new Main());
	} catch (IOException e) {
	    e.printStackTrace();
	}
	graphe = new Graphe("graphe");
	sommet = new Sommet("1");
	sommet.setVisible(false);
	graphe.ajouterSommet(sommet);
	
	commande = new CommandeAfficherSommet(sommet, graphe, panneauGraphique);
    }
    
    @Test
    public void afficherSommet() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertFalse(sommet.isVisible());
	commande.executer();
	assertTrue(sommet.isVisible());
    }

    private void testerDefaire() {
	commande.defaire();
	assertFalse(sommet.isVisible());
    }
}
