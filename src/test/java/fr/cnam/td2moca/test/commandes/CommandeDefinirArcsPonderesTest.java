package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.commandes.CommandeDefinirArcsPonderes;
import fr.cnam.td2moca.model.Graphe;

public class CommandeDefinirArcsPonderesTest {
    private static Graphe graphe;
    private static CommandeDefinirArcsPonderes commande;
    
    @BeforeClass
    public static void init() {
	graphe = new Graphe("graphe");
	graphe.setPonderationArc(false);
	
	commande = new CommandeDefinirArcsPonderes(graphe, true);
    }
    
    @Test
    public void definirArcsPonderes() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertFalse(graphe.isPonderationArc());
	commande.executer();
	assertTrue(graphe.isPonderationArc());
    }

    private void testerDefaire() {
	commande.defaire();
	assertFalse(graphe.isPonderationArc());
    }
}
