package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.CommandeAfficherTousLesSommets;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class CommandeAfficherTousLesSommetsTest {
    private static Graphe graphe;
    private static Sommet s1;
    private static Sommet s2;
    private static Sommet s3;
    private static PanneauGraphique panneauGraphique;
    private static CommandeAfficherTousLesSommets commande;
    
    @BeforeClass
    public static void init() {
	try {
	    panneauGraphique = new PanneauGraphique(new Main());
	} catch (IOException e) {
	    e.printStackTrace();
	}
	graphe = new Graphe("graphe");
	s1 = new Sommet("1");
	s2 = new Sommet("2");
	s3 = new Sommet("3");
	
	s1.setVisible(false);
	s2.setVisible(false);
	s3.setVisible(true);
	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	graphe.ajouterSommet(s3);
	
	commande = new CommandeAfficherTousLesSommets(graphe, panneauGraphique);
    }
    
    @Test
    public void afficherTousLesSommets() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertFalse(s1.isVisible());
	assertFalse(s2.isVisible());
	assertTrue(s3.isVisible());
	commande.executer();
	assertTrue(s1.isVisible());
	assertTrue(s2.isVisible());
	assertTrue(s3.isVisible());
    }

    private void testerDefaire() {
	commande.defaire();
	assertFalse(s1.isVisible());
	assertFalse(s2.isVisible());
	assertTrue(s3.isVisible());
    }
}
