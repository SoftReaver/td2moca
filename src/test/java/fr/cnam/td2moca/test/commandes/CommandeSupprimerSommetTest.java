package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.CommandeSupprimerSommet;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class CommandeSupprimerSommetTest {
    private static Graphe graphe;
    private static Sommet s1;
    private static Sommet s2;
    private static PanneauGraphique panneauGraphique;
    private static CommandeSupprimerSommet commande;
    
    @BeforeClass
    public static void init() {
	try {
	    panneauGraphique = new PanneauGraphique(new Main());
	} catch (IOException e) {
	    e.printStackTrace();
	}
	graphe = new Graphe("graphe");
	s1 = new Sommet("1");
	s2 = new Sommet("2");

	graphe.ajouterSommet(s1);
	graphe.ajouterSommet(s2);
	
	commande = new CommandeSupprimerSommet(s2, graphe, panneauGraphique);
    }
    
    @Test
    public void supprimerSommet() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertEquals(2, graphe.getListeSommets().size());
	assertTrue(graphe.getListeSommets().contains(s2));
	commande.executer();
	assertEquals(1, graphe.getListeSommets().size());
	assertFalse(graphe.getListeSommets().contains(s2));
    }

    private void testerDefaire() {
	commande.defaire();
	assertEquals(2, graphe.getListeSommets().size());
	assertTrue(graphe.getListeSommets().contains(s2));
    }
}
