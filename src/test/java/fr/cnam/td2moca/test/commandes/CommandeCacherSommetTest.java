package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.Main;
import fr.cnam.td2moca.commandes.CommandeCacherSommet;
import fr.cnam.td2moca.gui.PanneauGraphique;
import fr.cnam.td2moca.model.Graphe;
import fr.cnam.td2moca.model.Sommet;

public class CommandeCacherSommetTest {
    private static Graphe graphe;
    private static Sommet sommet;
    private static PanneauGraphique panneauGraphique;
    private static CommandeCacherSommet commande;
    
    @BeforeClass
    public static void init() {
	try {
	    panneauGraphique = new PanneauGraphique(new Main());
	} catch (IOException e) {
	    e.printStackTrace();
	}
	graphe = new Graphe("graphe");
	sommet = new Sommet("1");
	sommet.setVisible(true);
	graphe.ajouterSommet(sommet);
	
	commande = new CommandeCacherSommet(sommet, graphe, panneauGraphique);
    }
    
    @Test
    public void cacherSommet() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertTrue(sommet.isVisible());
	commande.executer();
	assertFalse(sommet.isVisible());
    }

    private void testerDefaire() {
	commande.defaire();
	assertTrue(sommet.isVisible());
    }
}
