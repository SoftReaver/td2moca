package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.commandes.CommandeRenommerGraphe;
import fr.cnam.td2moca.model.Graphe;

public class CommandeRenommerGrapheTest {
    private static Graphe graphe;
    private static CommandeRenommerGraphe commande;
    
    @BeforeClass
    public static void init() {
	graphe = new Graphe("ancien");
	graphe.setOriente(false);
	
	commande = new CommandeRenommerGraphe(graphe, "nouveau");
    }
    
    @Test
    public void renommerGraphe() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertEquals("ancien", graphe.getNom());
	commande.executer();
	assertEquals("nouveau", graphe.getNom());
    }

    private void testerDefaire() {
	commande.defaire();
	assertEquals("ancien", graphe.getNom());
    }
}
