package fr.cnam.td2moca.test.commandes;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.cnam.td2moca.commandes.CommandeModifierPositionSommets;
import fr.cnam.td2moca.model.Sommet;

public class CommandeModifierPositionSommetsTest {
    private static ArrayList<Sommet> listeSommets;
    private static Sommet s1;
    private static Sommet s2;
    private static Sommet s3;
    private static int x1;
    private static int x2;
    private static int y1;
    private static int y2;
    private static CommandeModifierPositionSommets commande;
    
    @BeforeClass
    public static void init() {
	s1 = new Sommet("1");
	s2 = new Sommet("2");
	s3 = new Sommet("3");
	
	s1.x = 500;
	s1.y = 100;
	s2.x = 250;
	s2.y = 0;
	s3.x = 85;
	s3.y = 70;
	
	listeSommets = new ArrayList<Sommet>();
	listeSommets.add(s1);
	listeSommets.add(s2);
	listeSommets.add(s3);
	
	x1 = 200;
	x2 = 300;
	y1 = 200;
	y2 = 100;
	
	commande = new CommandeModifierPositionSommets(x1, y1, x2, y2, listeSommets);
    }
    
    @Test
    public void modifierPositionSommets() {
	testerExecuter();
	testerDefaire();
    }
    
    private void testerExecuter() {
	assertEquals(500, (int) s1.x);
	assertEquals(100, (int) s1.y);
	assertEquals(250, (int) s2.x);
	assertEquals(0, (int) s2.y);
	assertEquals(85, (int) s3.x);
	assertEquals(70, (int) s3.y);
	commande.executer();
	assertEquals(600, (int) s1.x);
	assertEquals(0, (int) s1.y);
	assertEquals(350, (int) s2.x);
	assertEquals(-100, (int) s2.y);
	assertEquals(185, (int) s3.x);
	assertEquals(-30, (int) s3.y);
    }

    private void testerDefaire() {
	commande.defaire();
	assertEquals(500, (int) s1.x);
	assertEquals(100, (int) s1.y);
	assertEquals(250, (int) s2.x);
	assertEquals(0, (int) s2.y);
	assertEquals(85, (int) s3.x);
	assertEquals(70, (int) s3.y);
    }
}
