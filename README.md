# TD2Moca : Dessin de graphe
Application pour déssiner des graphes et appliquer des algorithme de la théorie des graphges.
Développé dans un but pédagogique en Java et avec l'aide de Swing pour les composants graphiques.
Les librairies suivantes ont été utilisées :
 - JDom à été utilisé pour l'écriture et lecture de fichier XML
 - JUnit 4 pour les tests unitaires

### Compiler et démarrer le projet

**Prérequis :**
Avoir [Maven](https://maven.apache.org/download.cgi), [Oracle JDK 11](https://www.oracle.com/fr/java/technologies/javase-jdk11-downloads.html) et [Java (JVM)](https://www.java.com/fr/download/manual.jsp) d'installés

Le projet utilise Maven comme gestionnaire de dépendance et gestionnaire de build. La configuration de Maven pour le projet se fait dans le fichier  **pom.xml** .

Pour compiler et enpaqueter l'application, placez vous à la racine du projet puis tapez la commande : <br>

 ```bash
 $ mvn clean package
 ```
 
 A l'issue du processus, deux JAR on été générés dans le dossier **target/** :
  - td2moca-&lt;version&gt;.jar
  - td2moca-&lt;version&gt;-jar-with-dependencies.jar
  
Seul le deuxième permet d'être exécuté en total autonomie à l'aide de la commande :

EXEMPLE :<br>

```bash
 $ java -jar td2moca-1.0-jar-with-dependencies.jar
```

# L'arborescence des fichiers

	\_src/main/java/			<== Contient les sources du projet
	|	\_<main_package>		<== Point d'entrée de l'application et classes d'initialisation (en générale on y touche pas sauf exception).
	|		\_actions/       	<== Contient les actions. Les actions sont des fonctionnalitées visibles par l'utilisateur et qui ont concience de l'existance du GUI (sorte de mini controlleurs)
	|		\_algos			<== Contient tous les algorithmes de graphe (parcours BFS, DFS, coloriage, etc....)
	|		\_commandes/		<== Les commandes sont associées à une action. Le role de la commande est d'agir sur le model directment. Une commande doit avoir la factulté de pouvoir être annulée (revenir à l'état précédent) et n'a aucune concience du GUI.
	|		\_gui/			<== Contient les différents composants graphique intégré dans la fenêtre de l'application
	|		\_handlers/		<== Contient les listeners d'évennement (sémantique Java Swing)
	|		\_io/			<== Les java bean (le modèle de données).
	|		\_model/		<== Le modèle du domain
	|		\_Invocateur.java	<== Classe responsable de garder un historique de toutes les commandes executées
	|		\_Main.java		<== Classe principale et contexte de l'application (point d'entrée)
	\_src/mains/resources/
	|	\_application-prod.properties	<== Configuration pour l'environnement de PROD
	\_src/test/java/			<== Contient tous les tests unitaires
	|	\_<packages>			<== Regrouper à convenance les tests par theme

# Documentation

Voir le [WIKI](https://gitlab.com/SoftReaver/td2moca/-/wikis/home).


